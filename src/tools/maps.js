import moment from "moment-mini";

class QeuryBuilder {
	constructor(props) {
		this.page = 1;
		this.equals = {};
		this.contains = {};
		this.range = {};
		this.orderBy = {};
		this.nullables = { cleaners: "IS NULL", admin: "IS NULL" };
	}

	setPage(data) {
		if (data && data.page) this.page = data.page;
		return this.page;
	}
	setEquals(data) {
		switch (data && data.equals) {
			case "interval.id":
				this.equals = { "interval.id": data.equals };
				break;
			case "booking.id":
				this.equals = { "booking.id": data.id };
				break;
			default:
				break;
		}
		return this.equals;
	}
	setContains(data, type) {
		if (type === "user") {
			switch (data && data.searchBy) {
				case "Name":
					this.contains = { "user.full_name": data.text };
					break;
				case "Address":
					this.contains = { "user.full_address": data.text };
					break;
				case "ID":
					this.contains = { "user.id": data.text };
					break;
				case "booking.id":
				default:
					break;
			}
			return this.contains;
		} else if (type === "booking") {
			if (data && data.id) 
				this.contains = { "booking.id": data.id };

			switch (data && data.searchBy) {
				case "Name":
					this.contains = { "booking.comment": data.text };
					break;
				case "ID":
					this.contains = { "booking.id": data.text };
					break;
				default:
					break;
			}
			return this.contains;
		}
	}
	setRange(data, type) {
		if (data && data.dateMin)
			switch (type) {
				case "user":
					this.range = { "user.created_at": { min: data.dateMin, max: data.dateMax } };
					break;
				case "interval":
					this.range = { "interval.time": { min: data.dateMin, max: data.dateMax } };
					break;
				default:
					break;
			}
		return this.range;
	}
	setOrederBy(data) {
		if (data && data.order)
			this.orderBy = { "user.firstName": "ASC" };
		else
			this.orderBy = { "user.firstName": "DESC" };

		return this.orderBy;
	}
	setNullables(data) {
		if (data && data.type === "cleaners")  
			this.nullables = { cleaners: "IS NOT NULL" };
		if (data && data.type === "admin")  
			this.nullables = { admin: "IS NOT NULL" };

		return this.nullables;
	}

}

export const checkRequestUserData = (data) => {
	const build = new QeuryBuilder();
	// const equals = 				data && data.equals ? { [data.equals.name]: data.equals.val } : {};
	const page =		build.setPage(data)
	const range = 		build.setRange(data, "user")
	const orderBy = 	build.setOrederBy(data)
	const contains = 	build.setContains(data, "user")
	const nullables = 	build.setNullables(data)
	// debugger
	return checkRequestProps({page, contains, range, orderBy, nullables});
}

export const checkRequestBookingData = (data) => {
	const build = new QeuryBuilder();

    const page =		build.setPage(data)
	const range = 		build.setRange(data, "interval")
	const equals =		build.setEquals(data)
	const contains = 	build.setContains(data, "booking")
    // debugger
    return checkRequestProps({ page, equals, contains, range });
}

const checkRequestProps = (data) => {
    const { page, equals, contains, range, orderBy, nullables } = data;
    return (
		{
			"page": page,
			"countPerPage": 15,
			"equals": equals,
			"contains": contains,
			"orderBy": orderBy,
			"range": range,
			"nullables": {
				"cleaner.id": nullables && nullables.cleaners,
				"admin.id": nullables && nullables.admin
			}
		}
	) 
}

export const centToPrice = (data, array) => {
	if (array) {
		return data.map((obj, k) => {
			let converted = (obj.amount / 100).toFixed(2);
			return { ...obj, amount: converted };
		});
	} else {
		return (data / 100).toFixed(2);
	}
};

export const sortSelectedCalendarDaysIntervals = (selectedDays, calendar) => {
	//check day by intervals
	let selectedDaysIntervals = [];
	selectedDays.map((obj, k) => {
		//get data of selected day and compare with array days from props
		const stringCurrDay = obj.toLocaleDateString();
		let dayIntervals = calendar.filter(interv => new Date(interv.time * 1000).toLocaleDateString() === stringCurrDay);
		if (dayIntervals.length===0)
			return null
		else
			return selectedDaysIntervals = [...selectedDaysIntervals, ...dayIntervals];
	});
	return selectedDaysIntervals;
}


export const sortSelectedDaysIntervals = (selectedDays, availableArr, reservedArr) => {
	//check day by intervals
	let selectedDaysIntervals = {
		available: [],
		reserved: []
	};
	selectedDays.map((obj, k) => {
		//get data of selected day and compare with array days from props
		const stringCurrDay = obj.toLocaleDateString();
		let available = availableArr.filter(av => new Date(av.time * 1000).toLocaleDateString() === stringCurrDay);
		let reserved = reservedArr.filter(re => new Date(re.time * 1000).toLocaleDateString() === stringCurrDay);
		return selectedDaysIntervals = { 
			available: [...selectedDaysIntervals.available, ...available], 
			reserved: [...selectedDaysIntervals.reserved, ...reserved] 
		}
	});
	return selectedDaysIntervals;
}

export const sortDifferenceOfReserve = (data , num) => {
	if (!data) return
	const updArr = data.map((obj, k) => {
		if (obj.duration < 14400) {
			return { ...obj, hours: 4 };
		} else if (14400 <= obj.duration && obj.duration < 28800) {
			return { ...obj, hours: 8 };
		} else if (28800 <= obj.duration && obj.duration <= 43200) {
			return { ...obj, hours: 12 };
		} else if (43200 < obj.duration ) {
			return { ...obj, hours: 24 };
		}
		return obj
	})
	switch (num) {
		case 4: 
			return updArr
				.filter((obj, k) => obj.hours === 4)
				.map((obj, k) => new Date(obj.time * 1000));
		case 8: 
			return updArr
				.filter((obj, k) => obj.hours === 8)
				.map((obj, k) => new Date(obj.time * 1000));
		case 12: 
			return updArr
				.filter((obj, k) => obj.hours === 12)
				.map((obj, k) => new Date(obj.time * 1000));
		case 24: 
			return updArr
				.filter((obj, k) => obj.hours === 24)
				.map((obj, k) => new Date(obj.time * 1000));
		default: 
			break;
	}
}

export const convertUnixToStrIntervalCurrDay = (data) => {
	if (data) {
		return data.map((obj, k) => {
			const day = new Date(obj.time*1000)
			const start = obj.time;
			const dur = obj.duration;
			const end = start + dur;
			const fullStartTime = moment.unix(start).format("ddd, Do, MMMM: hh:mm a");
			const startTime = moment.unix(start).format("ddd, Do: hh:mm a");
			const endTime = moment.unix(end).format("hh:mm a");

			const paymentStatus = obj && obj.paymentStatus ? obj.paymentStatus : null;

			return { id: obj.id, day, fullStartTime, startTime, endTime, paymentStatus };
		});
	} return data = [];
}

export const getObjUnixIntervalByDateAndTime = (date, start, end) => {
	const year = moment(date).year() //date.getFullYear();
	const month = moment(date).month() //date.getMonth();
	const day = moment(date).date() //date.getDate();
	
	const Shour = moment(start).hour() //start.getHours();
	const Smin = moment(start).minute() //start.getMinutes();
	// const Ssec = moment(start).second() //start.getSeconds();

	const Ehour = moment(end).hour() //end.getHours();
	const Emin = moment(end).minute() //end.getMinutes();
	// const Esec = moment(end).second() //end.getSeconds();

	const startDay = new Date(year, month, day, Shour, Smin, 0);
	const endDay = new Date(year, month, day, Ehour, Emin, 0);

	const unixStartDay = Number(new Date(startDay / 1000));
	const unixEndDay = Number(new Date(endDay / 1000));
	const unixDuration = Number(unixEndDay - unixStartDay);

	return { time: unixStartDay, duration: unixDuration, start: startDay };
}

export const getFirstLastDaysByMonth = (nextmonth) => {
	const date = new Date();
	const year = moment(date).year();
	const month = nextmonth ? nextmonth : moment(date).month();
	const firstDay = new Date(Date.UTC(year, month, 1, 0, 0, 0));
	const lastDay = new Date(Date.UTC(year, month + 1, 0, 0, 0, 0));
	const unixFirstDay = firstDay / 1000;
	const unixLastDay = lastDay / 1000 + 86399; // last day = day 00:00:00 + 23h 59 min 59 sec
	// debugger
	return { first: unixFirstDay, last: unixLastDay, month: firstDay };
};

export const objectByIdFromArray = (id, array, key) => {
	if(!key) key = 'id';
	id = Number(id);
	if(array && array.length) {
		return array.filter(obj => Number(obj[key]) === id)[0] || null;
	}
	return null
}
export const objectByStringIdFromArray = (key, array) => {
    for (let i=0; i < array.length; i++) {
		if (array[i].name === key) {
            return array[i];
        }
    }
}

export const emptyHouse = {
	name: "",
    address: "",
    flat: "",
	type: "detached",
	floors: 1,
	reception: 1,
	bedrooms: 1,
	bathrooms: 1,
	isBox: false,
	isConservatory: false,
	isDining: false,
	isOffice: false,
	isPlay: false,
	isUtility: false,
};
export const emptyProfile = {
	sex: "",
	contactEmail: "",
	email: "",
	password: "",
	nickName: "",
	firstName: "",
	lastName: "",
	address: "",
	flat: "",
	phone: "",
};
export const emptyPrice = {
	amount: 1,
	tag: "aditional",
	isMultiValued: false,
	maxValue: 1,
	minValue: 1,
	name: "New Price",
	description: "Price desc.",
};
