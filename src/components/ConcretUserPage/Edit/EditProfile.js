import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styled from "styled-components";

import { Form, Input, Row, Col, Select, Button } from 'antd';

import { editUserById } from "../../../store/actions/users";

const FormItem = Form.Item;
const Option = Select.Option;
const ButtonGroup = Button.Group;

const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
`;

class EditProfile extends Component {
	constructor(props) {
		super(props)
		this.state = {
			...this.props.data
		}
	}
	
	handleChangeText = (e) => {
		this.setState({ ...this.state, [e.target.name]: e.target.value });
	}
	handleChangeSex = (value) => {
		this.setState({ ...this.state, sex: value });
	}
	handleSubmit = () => {
		this.props.editUserById(this.state, this.state.id);
		this.handleGoBack()
	}
	handleGoBack = () => this.props.cancelEdit();

	render() {
		const { firstName, lastName, sex, contactEmail, phone, address, flat } = this.state;
		
		return (
			<Div>
				<Form>
					<FormItem label="Name" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Row gutter={8}>
							<Col span={12}>
								<Input name="firstName" value={firstName} onChange={this.handleChangeText} title="First name" />
							</Col>
							<Col span={12}>
								<Input name="lastName" value={lastName} onChange={this.handleChangeText} title="Last name" />
							</Col>
						</Row>
					</FormItem>
					<FormItem label="Gender" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Select onChange={this.handleChangeSex} value={sex} title="Gender" style={{width: "100%"}}>
							<Option value="male">Male</Option>
							<Option value="female">Female</Option>
						</Select>
					</FormItem>
					<FormItem label="E-mail" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Input name="contactEmail" value={contactEmail} onChange={this.handleChangeText} title="E-mail" />
					</FormItem>
					<FormItem label="Phone" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Input name="phone" value={phone} onChange={this.handleChangeText} title="Phone" />
					</FormItem>
					<FormItem label="Address" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Input name="address" value={address} onChange={this.handleChangeText} title="Address" />
					</FormItem>
					<FormItem label="Flat" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Input name="flat" value={flat} onChange={this.handleChangeText} title="Flat" />
					</FormItem>
					<ButtonGroup size="large" style={{ display: "flex", justifyContent: "center" }}>
						<Button onClick={this.handleGoBack}>
							Cancel
						</Button>
						<Button htmlType="submit" type="primary" onClick={this.handleSubmit}>
							Save
						</Button>
					</ButtonGroup>
				</Form>
			</Div>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ editUserById }, dispatch)
)(EditProfile);