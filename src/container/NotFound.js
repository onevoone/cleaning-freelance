import React from "react";
import styled from "styled-components";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button, Icon } from "antd";

const Div = styled.div`
	display: flex;
	justify-content: space-around;
	align-items: center;
	flex-direction: column;
	height: 200px;
	margin-top: 100px;
`

const NotFound = (router) => {
	return (
		<Div>
			<Icon type="frown-o" style={{ fontSize: 48, marginBottom: 16 }} />
			<h3 style={{fontFamily: "cursive"}}>404 Not Found</h3>
			<Button type="primary" onClick={() => router.router.goBack()} ghost><Icon type="rollback" />Go back</Button>
		</Div>
	)
}
export default connect(
	(state, ownProps) => {
		return {
			router: ownProps.router
		}
	}, dispatch => bindActionCreators({}, dispatch)
)(NotFound)