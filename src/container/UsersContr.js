// UsersContr.js
import React, { Component } from 'react';
import { bindActionCreators } from 'redux' 
import { connect } from 'react-redux';

import UsersTable from '../components/UsersTable';

import { fetchUsers, changeViewTab, deleteProfile } from "../store/actions/users";

class UsersContr extends Component {
	constructor(props) {
		super(props);
		this.handleOpenProfile = this.handleOpenProfile.bind(this);
	}

	componentDidMount() {
		if (this.props.users === null) {
			this.props.fetchUsers();
		}
	}

	handleOpenProfile(userId) {
		this.props.changeViewTab("profile")
		this.props.router.push(`/user/id/${userId}`);
	}
	handleDeleteProfile = (userId) => {
		this.props.deleteProfile(userId);
	}
	handleChangePage = (page) => {
		let reqProps = { page };
		this.props.fetchUsers(reqProps);
	}
	handleSearch = (search) => {
		this.props.fetchUsers(search);
	}

	render() {
		return (
			<UsersTable
				users={this.props.users}
				openProfile={this.handleOpenProfile}
				page={this.handleChangePage}
				search={this.handleSearch}
				deleteProfile={this.handleDeleteProfile}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			users: state.users.users_array,
			session: state.session,
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ fetchUsers, changeViewTab, deleteProfile }, dispatch)
)(UsersContr);