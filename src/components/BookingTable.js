// BookingTable.js
import React from "react";
import { Table, Icon, Button, Pagination } from "antd";
import styled from "styled-components";

import SearchForm from "./SearchForm";

const DivPagination = styled.div`
	display: flex;
	justify-content: center;
	margin: 40px 0 50px 0;
`;
const H1 = styled.h1`
	width: 170px;
`;

export default ({ bookingArr, openBooking, page, search }) => {

	const handleSearch = (data) => search(data);
	const onChangePage = (num) => page(num);

	const columns = [
		{ title: "Id", dataIndex: "id" },
		{ title: "Status", dataIndex: "status" },
		{ title: "Comment", dataIndex: "comment" },
		{
			title: "Action",
			render: booking => (
				<Button
					type="primary"
					ghost
					size="small"
					onClick={() => openBooking(booking.id)}
				>
					<Icon type="profile" />View
				</Button>
			)
		}
	];
	return (
		<div style={{ marginTop: "20px" }}>
			<div>
				<H1>
					Booking
				</H1>
				<SearchForm find={handleSearch} />
			</div>
			<Table
				columns={columns}
				rowKey="id"
				pagination={false}
				loading={bookingArr && bookingArr.items ? false : true}
				dataSource={bookingArr && bookingArr.items}
				scroll={{ x: 650 }}
			/>
			<DivPagination>
				<Pagination
					onChange={onChangePage}
					current={bookingArr && bookingArr.page}
					defaultCurrent={6}
					total={bookingArr && bookingArr.totalPages * 10}
				/>
			</DivPagination>
		</div>
	);
};
