//Login.js
import React, { Component } from 'react';
import { bindActionCreators } from 'redux' 
import { connect } from 'react-redux'; 

import LoginForm from '../components/LoginForm';
import { login } from '../store/actions/session';
import { clearError } from "../store/actions/message";
import { isSessionDataValid } from '../tools/sessionValidation';

class Login extends Component {	
	constructor (props){
		super(props);
		this.handleLogin = this.handleLogin.bind(this);
		this.redirectIfValidSession = this.redirectIfValidSession.bind(this);
	}
	componentWillMount() {
		this.redirectIfValidSession(this.props.router, this.props.session);
	}
	componentWillReceiveProps(nextProps) {
		this.redirectIfValidSession(nextProps.router, nextProps.session);
	}
	redirectIfValidSession(router, session) {
		// if(isSessionDataValid(session) && !isSessionDeleted(session)) {
		if (isSessionDataValid(session)) {
			this.props.clearError();
			router.replace('/users');
		}
	}
	handleLogin(mail, pass) {
		return this.props.login(mail, pass);
	}
	render() {
		return (
			<LoginForm login={this.handleLogin} error={this.props.error} />
		)
	}
}

export default connect(
	(state, ownProps) => {
		return {
			session: state.session.data,
			error: state.message.data,
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ login, clearError }, dispatch)
)(Login);