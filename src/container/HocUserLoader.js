// HocUsersLoader.js
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styled from "styled-components";
import path from "ramda/src/path";
import Spin from "antd/lib/spin";
import { Icon, Button, Tabs, Modal, Select } from "antd";

import {
	fetchUserById,
	fetchUserHousesById,
	getUserIntervalsById
} from "../store/actions/users";
import { fetchPrice } from "../store/actions/price";

import ConcretUserContr from "./ConcretUserContr";
import { getFirstLastDaysByMonth } from "../tools/maps";

const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
`;

const HocUserLoader = ConcretUserContr =>
class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false
		};
	}

	componentDidMount() {
		const userId = this.props.router.params.userId;
		this.fetchRequest(userId);
	}
	componentWillReceiveProps(nextProps) {
		const prevId = path(["id"], this.props.user);
		const nextId = Number(nextProps.router.params.userId);
		let prevIsCleanerApproved = path(["isCleanerApproved"], this.props.user);
		let nextIsCleanerApproved = path(["isCleanerApproved"], nextProps.user);
		if (prevId) {
			if (nextId !== prevId && !this.state.loading) {
				this.fetchRequest(nextId);
			}
		}
		// if cleaner from not approved to approved, fetch new intervals
		if (prevIsCleanerApproved===false && nextIsCleanerApproved===true) {
			this.fetchRequest(nextId);
		}
	}

	fetchRequest = async (userId) => {
		await this.setState({loading: true});
		await this.props.fetchUserById(userId);
		this.props.fetchUserHousesById(userId);
		
		let isCleaner = path(["cleanerId"], this.props.user);
		let isCleanerApproved = path(["isCleanerApproved"], this.props.user);
		isCleaner = isCleaner !== undefined ? isCleaner : null;
		isCleanerApproved = isCleanerApproved !== undefined ? isCleanerApproved : null;
		if (isCleaner && isCleanerApproved) {
			const days = getFirstLastDaysByMonth();

			await Promise.all([
				this.props.getUserIntervalsById(days.first, days.last, userId),
				this.props.fetchPrice()
			])
		}
		await this.setState({ loading: false });
	}

	render() {
		return (
			<div>
				{
					this.state.loading
					? <Loading><Spin /></Loading>
					: <ConcretUserContr router={this.props.router} />
				}
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			user: state.users.user,
			intervals: state.users.intervals,
			user_houses: state.users.user_houses,
			error: state.message.data,
			router: ownProps.router
		};
	},
	dispatch =>
		bindActionCreators(
			{
				fetchUserById,
				fetchUserHousesById,
				getUserIntervalsById,
				fetchPrice
			},
			dispatch
		)
)(HocUserLoader(ConcretUserContr));
