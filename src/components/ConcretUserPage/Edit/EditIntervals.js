import React, { PureComponent } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Spin, Icon, TimePicker, Button, List, Alert } from "antd";
import styled from 'styled-components';
import path from "ramda/src/path";
import moment from "moment-mini";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";

import "../../../css/Intervals.css";
import {
	getUserIntervalsById,
	putUserIntervalsById,
	deleteUserIntervalsById
} from "../../../store/actions/users";
import {
	convertUnixToStrIntervalCurrDay,
	getObjUnixIntervalByDateAndTime,
	sortSelectedCalendarDaysIntervals,
	getFirstLastDaysByMonth
} from "../../../tools/maps";

const SetTimeIntervals = styled.div`
	visibility: ${props => (props.show.length !== 0 ? "visible" : "hidden")};
	width: 400px;
`;
const CustomList = styled(List)`
	margin-top: 30px;
	max-height: 450px;
	overflow-y: auto;
`;
const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
	height: 432px;
`;

function Navbar({ onPreviousClick, onNextClick, className }) {
	const nextMonth = () => onNextClick();
	const prevMonth = () => onPreviousClick();
	return (
		<div className={className} style={{position: "absolute", right: "20px", top: "15px"}}>
			<Button style={{marginRight: "3px"}} shape="circle" icon="left" onClick={prevMonth} />
			<Button style={{marginLeft: "3px"}} shape="circle" icon="right" onClick={nextMonth} />
		</div>
	);
}

class EditIntervals extends PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			error: null,
			selectedDaysIntervals: [],
			selectedDays: [],
			intervals: {
				start: null,
				end: null,
			},
			month: moment().month(),
			enableRefresh: false
		}
	}

	componentDidMount() {
		// if the current month !== the month that was uploaded
		const available = this.props.intervals.available.length !== 0 && this.props.intervals.available[0].time * 1000;	
		const loadedMonth = moment(available).month();
		if (loadedMonth !== this.state.month) { 
			this.onMonthChange();
		}
	}

	componentWillReceiveProps(nextProps) {
		//	set downloaded days to selectedDaysIntervals to display in the Daypicker
		if (nextProps.intervals && nextProps.intervals.calendar) {  
			const sSDI = sortSelectedCalendarDaysIntervals(this.state.selectedDays, nextProps.intervals.calendar);
			this.setState({ selectedDaysIntervals: sSDI });
		}
	}

	setEmptyError = () => this.setState({ error: null });

	monthParams = (call) => {
		const days = getFirstLastDaysByMonth(this.state.month)
		const userId = this.props.userId;
		call(days.first, days.last, userId)
	}
	onMonthChange = async (date) => {
		// set the new number of the selected month, get all of its days and make a request to receive the data of the month
		const newSelectedMonth = moment(date).month();
		this.setState({ month: newSelectedMonth, selectedDays: [] }); //
		const days = getFirstLastDaysByMonth(newSelectedMonth);
		this.setState({ enableRefresh: true });
		await this.props.getUserIntervalsById(days.first, days.last, this.props.userId)
		this.setState({ enableRefresh: false });
	}
	deleteInterval = (e) => {
		const intervalId = e.target.value;
		this.monthParams((first, last, userId) => (
			this.props.deleteUserIntervalsById(first, last, userId, intervalId)
		))
	}
	selectTime = (val, name) => {
		const time = new Date(val);
		this.setState((state) => { return { intervals: {...state.intervals, [name]: time } }});
	}
	submitInterval = () => {
		const startTime = this.state.intervals.start;
		const endTime = this.state.intervals.end;
		this.monthParams((first, last, userId) => {
			if (startTime===null || endTime===null || startTime>endTime || !this.state.selectedDays.length) {
				this.setState({error: "Error of the entered data, check their correctness."});
				return
			} else {
				const newInterval = this.state.selectedDays.map((obj, k) => {
					return getObjUnixIntervalByDateAndTime(obj, startTime, endTime);
				})
				this.setState({ selectedDays: [] });
				this.props.putUserIntervalsById(first, last, userId, newInterval);
			}
		})
	}
	handleDayClick = (day, { selected }) => {
		//select or deselect day
		const { selectedDays } = this.state;
		const { intervals } = this.props;
		if (selected) {
			const selectedIndex = selectedDays.findIndex(selectedDay =>
				DateUtils.isSameDay(selectedDay, day)
			);
			selectedDays.splice(selectedIndex, 1);
		} else {
			selectedDays.push(day);
		}
		this.setState({ selectedDays });

		// set selectedDaysIntervals to display as list
		const sSDI = sortSelectedCalendarDaysIntervals(selectedDays, intervals.calendar);
		this.setState({ selectedDaysIntervals: sSDI });
	}
	
	render () {
		const calendar = path(["intervals", "calendar"], this.props);
		const modifiers = {
			calendar: calendar && calendar.map((obj, k) => new Date(obj.time*1000))
		}
		return (
			<div>
				{ this.state.error && (
				<Alert
					style={{width: "400px", margin: "0 auto 10px auto"}}
					message={this.state.error}
					type="error"
					closable
					onClose={this.setEmptyError}
				/>) }
				{ !this.state.enableRefresh && this.props.intervals && this.props.intervals.calendar !== null
					? <div className="intervalsContainer">
						<DayPicker
							selectedDays={this.state.selectedDays}
							onDayClick={this.handleDayClick}
							modifiers={modifiers}
							navbarElement={<Navbar />}
							onMonthChange={this.onMonthChange}
							month={getFirstLastDaysByMonth(this.state.month).month}
						/>
						<SetTimeIntervals show={this.state.selectedDays}>
							<div className="newTime">
								<TimePicker 
									style={{width: 150}} 
									placeholder="Start at" 
									use12Hours 
									format="h:mm a" 
									name="start" 
									onChange={(e)=>this.selectTime(e, "start")} 
								/>
								<TimePicker 
									style={{width: 150}} 
									placeholder="End at" 
									use12Hours 
									format="h:mm a" 
									name="end" 
									onChange={(e)=>this.selectTime(e, "end")} 
								/>
								<Button onClick={this.submitInterval} title="Add interval to selected days"><Icon type="plus-circle-o"/>Add</Button>
							</div>
							{
								this.state.selectedDaysIntervals.length !== 0
								? <CustomList
										style={{marginTop: "30px"}}
										header={<div style={{textAlign: "center"}}>Intervals by selected days</div>}
										size="small"
										bordered
										dataSource={convertUnixToStrIntervalCurrDay(this.state.selectedDaysIntervals)}
										renderItem={item => (
											<List.Item actions={	
												[<Button size="small" shape="circle" icon="close" title="Delete" value={item.id} onClick={this.deleteInterval} />]
											}>
												{<div>{item.startTime} - {item.endTime}</div>} 
											</List.Item>
										)}
									/>
								: null
							}
						</SetTimeIntervals>
					</div>
					: <Loading><Spin/></Loading>
				}
			</div>
		);
	}
}
export default connect(
	(state, ownProps) => {
		return {
			intervals: state.users.intervals
		};
	},
	dispatch =>
		bindActionCreators(
			{
				getUserIntervalsById,
				putUserIntervalsById,
				deleteUserIntervalsById
			},
			dispatch
		)
)(EditIntervals);
