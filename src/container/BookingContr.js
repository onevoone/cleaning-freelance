// BookingContr.js
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import BookingTable from "../components/BookingTable";
import { fetchBookingArr } from "../store/actions/booking";

class BookingContr extends Component {

	componentDidMount() {
		if (this.props.booking === null) {
			this.props.fetchBookingArr();
		}
	}

	handleChangePage = page => this.props.fetchBookingArr({page});

	handleOpenBooking = id => this.props.router.push(`/booking/id/${id}`);
	
	handleSearch = search => this.props.fetchBookingArr(search);

	render() {
		return (
			<BookingTable
				bookingArr={this.props.booking}
				openBooking={this.handleOpenBooking}
				page={this.handleChangePage}
				search={this.handleSearch}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			booking: state.booking.array,
			error: state.message.data,
			router: ownProps.router
		}
	},
	dispatch => bindActionCreators({ fetchBookingArr }, dispatch)
)(BookingContr);
