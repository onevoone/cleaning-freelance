import React from "react";
import { Icon, Tabs, Switch, Button } from "antd";
import path from "ramda/src/path";

import EditProfile from "./Edit/EditProfile";
import EditHouse from "./Edit/EditHouse";
import EditIntervals from "./Edit/EditIntervals";

import ViewHouse from "./View/House";
import ViewProfile from "./View/Profile";
import Map from "./View/Map";
import Intervals from "./View/Intervals";

import { emptyHouse, objectByIdFromArray, centToPrice } from "../../tools/maps";

const TabPane = Tabs.TabPane;

export function togglerCleanerApprove(props, handleApproveCleaner) {
	const { user } = props;

	if (user && user.cleanerId !== null) {
		return (
			<div style={{ margin: "10px 10px 10px 0" }}>
				<label>Approved cleaner: &nbsp;</label>
				<Switch
					checkedChildren={<Icon type="check" />}
					unCheckedChildren={<Icon type="cross" />}
					checked={user.isCleanerApproved}
					onChange={handleApproveCleaner}
				/>
			</div>
		);
	}
	return null;
}

export function serviceHourPrice(props, setPriceModal) {
	const HourPriceId = path(["serviceHourPriceId"], props.user);
	const time = path(["time"], props.price);

	const currTime = time && time.find((obj) => obj.id === HourPriceId);

	if (HourPriceId) {
		return (
			isCleanerAndApproved(props) && currTime &&
			<div style={{ margin: "10px 10px 10px 30px", display: "flex", alignItems: "center" }}>
				<label>Hour price: &nbsp;</label>
				{centToPrice(currTime.amount)} &pound;
				<Button size="small" type="dashed" style={{ marginLeft: 10 }} onClick={setPriceModal}>change</Button>
			</div>
		)
	} else {
		return (
			isCleanerAndApproved(props) &&
			<Button type="primary" size="small" style={{marginLeft: 20}} onClick={setPriceModal}>Set hour price</Button>
		)
	}
}

export function tabProfileRender(state, props, handleCancelEdit) {
	return (
		<TabPane tab="Profile" key="profile">
			{
				state.editProfile 
				? <EditProfile data={props.user} cancelEdit={handleCancelEdit} />
				: <ViewProfile data={props.user} />
			}
		</TabPane>
	);
}

export function tabHouseRender(state, props, handleCancelEdit) {
	const { houses, user } = props;
	const { editHouse } = state;

	if (houses && houses.length !== 0) {
		return houses.map((obj, k) => (
			<TabPane tab={`House ${k + 1}`} key="house">
				{
					editHouse 
					? <EditHouse
							data={objectByIdFromArray(obj.id, houses, "id")}
							cancelEdit={handleCancelEdit}
						/>
					: <ViewHouse data={objectByIdFromArray(obj.id, houses, "id")} />
				}
			</TabPane>
		));
	} else if (houses && houses.length === 0 && path(["cleanerId"], user) === null) {
		// if array is empty and user is not cleaner
		return (
			<TabPane tab="Add house" key="newHouse">
				<EditHouse
					data={emptyHouse}
					userId={path(["id"], user)}
					cancelEdit={handleCancelEdit}
				/>
			</TabPane>
		);
	} else {
		return null;
	}
}

export function tabIntervalsRender(state, props, showBooking) {
	return ( 
		isCleanerAndApproved(props) && 
		<TabPane tab="Intervals" key="intervals">
			{
				state.editIntervals 
				? <EditIntervals userId={path(["id"], props.user)} />
				: <Intervals userId={path(["id"], props.user)} showBooking={showBooking} />
			}
		</TabPane>
	);
}

export function tabMapRender(props) {
	const location = path(["user", "location"], props);
	return (
		location && isCleanerAndApproved(props) &&
			<TabPane tab="Map" key="map">
				<Map user={props.user} />
			</TabPane>
	);
}



const isCleanerAndApproved = (data) => {
	const cleaner = path(["user", "cleanerId"], data);
	const checkApprove = path(["user" ,"isCleanerApproved"], data);
	
	if (cleaner && checkApprove)
		return true
	else 
		return false
}