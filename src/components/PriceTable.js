import React, { Component } from "react";
import { Table, Icon, Button } from "antd";
import styled from "styled-components";
import { ModalDelete } from "./SmallModals";
import { centToPrice } from "../tools/maps";

import "../css/tables.css";

const H1 = styled.h1`
	width: 170px;
`;
const DivHourPrice = styled.h2`
	width: 300px;
	margin-bottom: 20px;
	& > span {
		font-weight: 500;
	}
`;

export default class PriceTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			loading: false
		}
	}

	handleDelPrice = (id, text) => {
		this.deleteItem = () => {
			this.props.deletePrice(id);
		}
		ModalDelete("price", text, this.deleteItem);
	}

	render() {
		const { price } = this.props;
		const columns = [
			{ title: "ID", dataIndex: "id", className: "table-col-id" },
			{ title: "Name", dataIndex: "name", className: "table-col-lname" },
			{ title: "Description", dataIndex: "description", className: "table-col-desc" },
			{ title: "Amount", dataIndex: "amount", className: "table-col-action" },
			{
				title: "Action",
				className: "table-col-action",
				render: service => (
					<Button size="small" onClick={() => this.handleDelPrice(service.id, service.name)}>
						<Icon type="delete" />Delete
					</Button>
				)
			}
		];
		
		return (
			<div style={{ marginTop: "20px" }}>
				<div>
					<H1>Settings</H1>
					<Button style={{ marginBottom: "20px" }} onClick={() => this.props.addPrice()}>
						<Icon type="calculator" />Add Price
					</Button>
				</div>
				<DivHourPrice><span>Prices of hours</span></DivHourPrice>
				<Table
					className="table-nested"
					columns={columns}
					rowKey="id"
					pagination={false}
					loading={price && price.time ? false : true}
					dataSource={price && centToPrice(price.time, "arr")}
					scroll={{ x: 650 }}
				/>

				<DivHourPrice style={{ marginTop: 100 }}><span>Prices of additional</span></DivHourPrice>
				<Table
					className="table-nested"
					columns={columns}
					rowKey="id"
					pagination={false}
					loading={price && price.aditional ? false : true}
					dataSource={price && centToPrice(price.aditional, "arr")}
					scroll={{ x: 650 }}
					style={{marginBottom: 100}}
				/>
			</div>
		)
	}
}
