import { setError as setMessage } from "./message";

import {
	fetchBookingArr as fetchBookingArrRequest,
	fetchBookingById as fetchBookingByIdRequest,
	rejectBookingById as rejectBookingByIdRequest,
	fetchBookingIntervalById as fetchBookingIntervalByIdRequest,
	editIntervalStatusById as editIntervalStatusByIdRequest,
	setFullPaidIntervalsById as setFullPaidIntervalsByIdRequest
} from "../../api/";

export const SET_ARRAY_BOOKING = "SET_ARRAY_BOOKING";
export const SET_CONCRET_BOOKING = "SET_CONCRET_BOOKING";
export const SET_BOOKING_INTERVALS = "SET_BOOKING_INTERVALS";

const setBookingArr = (obj) => {
	return {
		type: SET_ARRAY_BOOKING,
		payload: obj
	}
}
const setBooking = (obj) => {
	return {
		type: SET_CONCRET_BOOKING,
		payload: obj
	}
}
const setBookingIntervals = (obj) => {
	return {
		type: SET_BOOKING_INTERVALS,
		payload: obj
	}
}

export const fetchBookingArr = (data) => dispatch => {
	dispatch(setBookingArr(null));
	return fetchBookingArrRequest(data)
		.then(response => dispatch(setBookingArr(response)))
		.catch(error => dispatch(setMessage(error)))
}

export const fetchBookingById = (data) => dispatch => {
	dispatch(setBooking(null));
	return fetchBookingByIdRequest(data)
		.then(response => {
			dispatch(setBooking(response))
			return response;
		})
		.catch(error => dispatch(setMessage(error)))
}

export const fetchBookingIntervalById = (data) => dispatch => {
	return fetchBookingIntervalByIdRequest(data)
		.then(response => dispatch(setBookingIntervals(response)))
		.catch(error => dispatch(setMessage(error)));
}

export const editIntervalStatusById = (bookingId, intervalId, data) => dispatch => {
	return editIntervalStatusByIdRequest(bookingId, intervalId, data)
		.then(response => dispatch(setBookingIntervals(response)))
		.catch(error => dispatch(setMessage(error)));
}

export const setFullPaidIntervalsById = (cleanerId, bookingId) => dispatch => {
	dispatch(setBooking(null));
	return setFullPaidIntervalsByIdRequest(cleanerId, bookingId)
		.then(response => dispatch(setBooking(response)))
		.catch(error => dispatch(setMessage(error)));
}

export const rejectBookingById = (cleanerId, bookingId) => dispatch => {
	dispatch(setBooking(null));
	return rejectBookingByIdRequest(cleanerId, bookingId)
		.then(response => dispatch(setBooking(response)))
		.catch(error => dispatch(setMessage(error)));
}