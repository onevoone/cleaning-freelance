// ListIntervals.js
import React from "react";
import { Button } from "antd";
import styled from "styled-components";

import { Status } from "./StatusSpan";

import { convertUnixToStrIntervalCurrDay } from "../../tools/maps";
import "../../css/ConcretBookingPage.css";

const ListIntervals = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-wrap: wrap;
	max-width: 500px;

	& > span {
		margin: 5px 0;
		margin-right: 10px;
	}
	& > div {
		margin: 5px 0;
	}
`;

export default ({ intervals, intervalStatus, bookingStatus }) => {
	const changeIntervalStatus = (el) => {
		const id = Number(el.target.id);
		const reqBody = { "status": String(el.target.value) };
		intervalStatus(id, reqBody);
	};
	
	return (
		convertUnixToStrIntervalCurrDay(intervals).map(
			(obj, k) => (
				<ListIntervals key={k}>
					<span>{obj.fullStartTime} - {obj.endTime}</span>
					
					{
						bookingStatus === "pending" || bookingStatus === "accepted"
						? <div style={{ width: 200 }}>
							<span style={{ fontWeight: 500 }}>Status:</span>
							{
								obj.paymentStatus === "pending"
									? <Status orange>{obj.paymentStatus}</Status>
									: obj.paymentStatus === "paid"
										? <Status green>{obj.paymentStatus}</Status>
										: obj.paymentStatus === "failed"
											? <Status red>{obj.paymentStatus}</Status>
											: null
							}
							{
								obj.paymentStatus === "pending" || obj.paymentStatus === "failed"
									? <Button
										id={obj.id}
										size="small"
										type="dashed"
										value="paid"
										onClick={changeIntervalStatus}
									>
										Paid
									</Button>
									: obj.paymentStatus === "paid" 
										? <Button
											id={obj.id}
											size="small"
											type="dashed"
											value="pending"
											onClick={changeIntervalStatus}
										>
											Pending
										</Button>
									: null
							}
						</div>
						: null
					}
					
				</ListIntervals>
			)
		)
	)		
}
