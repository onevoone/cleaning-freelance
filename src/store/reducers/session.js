import { reedCookies, CHECK_LOGIN, CLEAR } from "../actions/session";

const initialState = {
	data: reedCookies()
};

export default function session(state = initialState, action) {
	switch(action.type) {
		case CHECK_LOGIN:
			return {
				...state,
				data: action.payload,
			}
		case CLEAR:
			return {
				data: null,
			}
		default:
			return state;	
	}
}
