// sessionValidation.js

export const isSessionDataValid = (session) => {
	if(!session) {
		return false
	}
	else if(!session.userId) {
		return false
	}
	else if(!session.authToken) {
		return false
	}
	else if(!session.refreshToken) {
		return false
	}
	else if(!session.scToken) {
		return false
	}
	else {
		return true
	}
}

export const isSessionExpire = (session, tReserve = 60) => {
	if (!session || !session.expiresIn) return false
	
	let start = Number(session.createdAt)
	let end = start + session.expiresIn - tReserve;
	let now = new Date().getTime() / 1000;
	
	if (end <= now)
		return true
	else
		return false
}