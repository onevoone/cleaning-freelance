import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Spin, Button, List, Icon } from "antd";
import styled from "styled-components";
import path from "ramda/src/path";
import moment from "moment-mini";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";

import "../../../css/Intervals.css";

import {
	getUserIntervalsById,
	rebuildMonthIntervals,
} from "../../../store/actions/users";

import {
	convertUnixToStrIntervalCurrDay,
	sortSelectedDaysIntervals,
	getFirstLastDaysByMonth,
	sortDifferenceOfReserve
} from "../../../tools/maps";

const SetTimeIntervals = styled.div`
	visibility: ${props => (props.show.length !== 0 ? "visible" : "hidden")};
	width: 400px;
`;
const CustomList = styled(List)`
	margin-top: 30px;
	max-height: 450px;
	overflow-y: auto;
`;
const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
	height: 432px;
`;
const IntervalItem = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	color: ${p => p.type === "reserved" ? "red" : null};
`;

function Navbar({ onPreviousClick, onNextClick, className }) {
	const nextMonth = () => onNextClick();
	const prevMonth = () => onPreviousClick();
	return (
		<div className={className} style={{ position: "absolute", right: "20px", top: "15px" }}>
			<Button style={{ marginRight: "3px" }} shape="circle" icon="left" onClick={prevMonth} />
			<Button style={{ marginLeft: "3px" }} shape="circle" icon="right" onClick={nextMonth} />
		</div>
	);
}

class Intervals extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedDaysIntervals: [],
			selectedDays: [],
			month: moment().month(),
			modifiers: "Available",
			enableRefresh: false
		};
	}

	componentDidMount() {
		const { intervals } = this.props.intervals;
		// if the current month !== the month that was uploaded
		const available = intervals && intervals.available.length !== 0 && intervals.available[0].time * 1000;	
		const loadedMonth = moment(available).month();
		if (loadedMonth !== this.state.month) {
			this.onMonthChange();
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.intervals && nextProps.intervals.available) {
			// set downloaded days to selectedDaysIntervals to display in the Daypicker
			const sSDI = sortSelectedDaysIntervals(this.state.selectedDays, nextProps.intervals.available, nextProps.intervals.reserved);
			this.setState({ selectedDaysIntervals: sSDI });
		}
	};

	onMonthChange = async (date) => {
		// set the new number of the selected month, get all of its days and make a request to receive the data of the month
		const newSelectedMonth = moment(date).month();
		this.setState({ month: newSelectedMonth }); // , selectedDays: [] });
		const day = getFirstLastDaysByMonth(newSelectedMonth);
		this.setState({ enableRefresh: true });
		await this.props.getUserIntervalsById(day.first, day.last, this.props.userId)
		this.setState({ enableRefresh: false });
	};

	handleDayClick = (day, { selected }) => {
		//select or deselect day
		const { selectedDays } = this.state;
		const { intervals } = this.props;
		if (selected) {
			const selectedIndex = selectedDays.findIndex(selectedDay =>
				DateUtils.isSameDay(selectedDay, day)
			);
			selectedDays.splice(selectedIndex, 1);
		} else {
			selectedDays.push(day);
		}
		this.setState({ selectedDays });

		// set selectedDaysIntervals to display as list
		const sSDI = sortSelectedDaysIntervals(selectedDays, intervals.available, intervals.reserved);
		this.setState({ selectedDaysIntervals: sSDI });
	};

	handleRefreshIntervals = () => this.onMonthChange();

	handleRebuildIntervals = async () => {
		const days = getFirstLastDaysByMonth(this.state.month);
		this.setState({ enableRefresh: true });
		await this.props.rebuildMonthIntervals(days.first, days.last, this.props.userId);
		this.setState({ enableRefresh: false });
	}
	
	handleRedirectToBooking = (event) => {
		this.props.showBooking(event.target.id);
	}

	render() {
		const availableArr = path(["intervals", "available"], this.props);
		const reservedArr = path(["intervals", "reserved"], this.props);
		const modifiers = {
			available: availableArr && availableArr.map((obj, k) => new Date(obj.time * 1000)),
			reserved_4: reservedArr && sortDifferenceOfReserve(reservedArr, 4),
			reserved_8: reservedArr && sortDifferenceOfReserve(reservedArr, 8),
			reserved_12: reservedArr && sortDifferenceOfReserve(reservedArr, 12),
			reserved_24: reservedArr && sortDifferenceOfReserve(reservedArr, 24)
		}

		const allSelectedCells = [];
		convertUnixToStrIntervalCurrDay(this.state.selectedDaysIntervals.available).forEach(
			element => {
				allSelectedCells.push({
					...element,
					type: "available"
				});
			}
		);
		convertUnixToStrIntervalCurrDay(this.state.selectedDaysIntervals.reserved).forEach(
			element => {
				allSelectedCells.push({
					...element,
					type: "reserved"
				});
			}
		);
		allSelectedCells.sort((a,b) => {
			if (a.day.getTime() > b.day.getTime()) return 1;
			else if (a.day.getTime() < b.day.getTime()) return -1;
			else return null;
		});
		
		return (
			<div>
				{/* {this.props.intervals && this.props.intervals.available !== null  */}
				{!this.state.enableRefresh 
				? (
					[<div key="1" style={{display: "flex", justifyContent: "flex-end"}}>
						<Button 
							onClick={this.handleRefreshIntervals} 
							disabled={this.state.enableRefresh}
							style={{marginRight: 30}}
							title="Refresh"
						>
						{
							this.state.enableRefresh
							? <Icon type="loading" />
							: <Icon type="reload" />
						}
						</Button>
						<Button onClick={this.handleRebuildIntervals}>Recalculate</Button>
					</div>,
					<div className="intervalsContainer" key="2">
						<DayPicker
							selectedDays={this.state.selectedDays}
							onDayClick={this.handleDayClick}
							modifiers={modifiers}
							navbarElement={<Navbar />}
							onMonthChange={this.onMonthChange}
							month={getFirstLastDaysByMonth(this.state.month).month}
						/>
						<SetTimeIntervals show={this.state.selectedDays}>
							{this.state.selectedDaysIntervals.length !== 0 ? (
								<CustomList
									style={{ marginTop: 20 }}
									header={<div style={{ textAlign: "center" }}>Availale intervals by selected days</div>}
									size="small"
									bordered
									dataSource={allSelectedCells}
									renderItem={item => (
										<List.Item>
											<IntervalItem type={item.type}>
												{item.startTime} - {item.endTime} {item.type === "reserved" ? "(booked)" : ""}
												{
													(item.type === "reserved")
													? <Button 
														type="dashed" 
														shape="circle" 
														icon="info" 
														size="small" 
														title="Show booking details"
														id={item.id} 
														onClick={this.handleRedirectToBooking}
													/>
													: null
												}
											</IntervalItem>
										</List.Item>
									)}
								/>
							) : null}
						</SetTimeIntervals>
					</div>]
				) : (
					<Loading><Spin /></Loading>
				)}
			</div>
		);
	}
}
export default connect(
	(state, ownProps) => {
		return {
			intervals: state.users.intervals
		};
	},
	dispatch =>
		bindActionCreators({ getUserIntervalsById, rebuildMonthIntervals },
			dispatch
		)
)(Intervals);
