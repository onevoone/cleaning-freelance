import { React } from "react";
import TestUtils from 'react-addons-test-utils';
// describe("test of authorization", () => {

// 	const testData = {
// 		email: "audi2014@gmail.com",
// 		password: "qwerty1234"
// 	};

// 	it("take data of current user", async () => {
// 		// expect(await)
// 	})
// })
class Wellcome extends React.Component {
	onClick() {
		this.props.someFunction(this.props.username);
	}

	render() {
		return (
			<div>
				<span onClick={this.onClick.bind(this)}>Wellcome {this.props.username}</span>
			</div>
		);
	}
}

describe('<Welcome />', () => {
	it('Renders wellcome message to user', () => {
		const onClickSpy = jest.fn();
		const username = 'Alice';

		const component = ReactTestUtils.renderIntoDocument(
			<Wellcome username={username} someFunction={onClickSpy} />
		);
		const span = TestUtils.findRenderedDOMComponentWithTag(
			component, 'span'
		);

		TestUtils.Simulate.click(span);

		expect(span.textContent).toBe(`Wellcome Alice`);
		expect(onClickSpy).toBeCalledWith(username);
	});
});