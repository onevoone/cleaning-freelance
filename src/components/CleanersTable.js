// CleanersTable.js
import React, { Fragment } from "react";
import { Table, Icon, Button, Pagination, Modal } from "antd";
import styled from "styled-components";

import SearchForm from "./SearchForm";
import "../css/tables.css";

const confirm = Modal.confirm;

const DivPagination = styled.div`
	display: flex;
	justify-content: center;
	margin: 40px 0 50px 0;
`;
const H1 = styled.h1`
	width: 170px;
`;

export default ({ addCleaner, openProfile, page, cleaners, search, deleteProfile }) => {

	const handleSearch = (data) => search(data);
	const onChangePage = (num) => page(num);

	const handledeleteProfile = (e) => {
		const id = Number(e.target.id);
		confirm({
			title: `Warning`,
			content: `Are you sure you want DELETE user with id: ${id}?`,
			okType: 'danger',
			okText: 'Yes',
			cancelText: 'No',
			onOk: () => deleteProfile(id)
		});
	}

	const columns = [
		{ title: "ID", dataIndex: "id", className: "table-col-id" },
		{ title: "First Name", dataIndex: "firstName", className: "table-col-fname" },
		{ title: "Last Name", dataIndex: "lastName", className: "table-col-lname" },
		{ title: "Email", dataIndex: "email", className: "table-col-email" },
		{
			title: "Action",
			className: "table-col-action",
			render: user => (
				<Fragment>
					<Button 
						type="primary" 
						size="small" 
						ghost 
						onClick={() => openProfile(user.id)} 
						style={{marginRight: 10, marginBottom: 10}}
					>
						<Icon type="profile" />View
					</Button>
					<Button 
						size="small" 
						id={user.id} 
						onClick={handledeleteProfile}
					>
						<Icon type="user-delete" />Delete
					</Button>
				</Fragment>
			)
		}
	];
	
	return (
		<div style={{ marginTop: "20px" }}>
			<div>
				<H1 className="title" style={{ width: "170px" }}>
					Cleaners
				</H1>
				<Button style={{ marginRight: "90px" }} onClick={() => addCleaner()}>
					<Icon type="user-add" />Add Cleaner
				</Button>
				<SearchForm find={handleSearch} />
			</div>
			<Table
				className="table-main"
				columns={columns}
				rowKey="id"
				pagination={false}
				loading={(cleaners && cleaners.items) ? false : true} 
				dataSource={cleaners && cleaners.items}
				scroll={{ x: 650 }}
			/>
			<DivPagination>
				<Pagination onChange={onChangePage} current={cleaners && cleaners.page} defaultCurrent={6} total={cleaners && cleaners.totalPages*10} />
			</DivPagination>
		</div>
	);
};
