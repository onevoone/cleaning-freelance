import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styled from "styled-components";
import { Form, Input, Col, Select, Checkbox, InputNumber, Button } from 'antd';

import { editUserHousesById } from "../../../store/actions/users";

const FormItem = Form.Item;
const Option = Select.Option;
const ButtonGroup = Button.Group;

const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
`;

class EditHouse extends Component {
	constructor(props) {
		super(props)
		this.state = {
			userId: this.props.userId,
			...this.props.data
		}
	}
	handleChangeText = (e) => {
		this.setState({ ...this.state, [e.target.name]: String(e.target.value) });
	}
	handleCheckbox = (e) => {
		this.setState({ ...this.state, [e.target.name]: Boolean(e.target.checked) });
	}
	handleChangeSelect = (value, name) => {
		this.setState({ ...this.state, [name]: value });
	}
	handleGoBack = () => {
		this.props.cancelEdit();
	}
	handleSubmit = () => {
		this.props.editUserHousesById([this.state], this.state.userId);
		this.handleGoBack()
	}

	render() {
		const { 
			name, address, flat, type, floors, reception, bedrooms, bathrooms, 
			isBox, isConservatory, isDining, isOffice, isPlay, isUtility 
		} = this.state;
		
		return (
			<Div>
				<Form onSubmit={this.handleSubmit} layout="vertical" style={{maxWidth: "380px"}}>
					<FormItem label="Name">
						<Input name="name" value={name} onChange={this.handleChangeText} />
					</FormItem>
					<FormItem label="Address">
						<Input name="address" value={address} onChange={this.handleChangeText} />
					</FormItem>
					<FormItem label="Flat">
						<Input name="flat" value={flat} onChange={this.handleChangeText} />
					</FormItem>
					<FormItem label="Type">
						<Select
							onChange={(e) => this.handleChangeSelect(e, 'type')}
							value={type}
						>
							<Option value="studio">Studio</Option>
							<Option value="flat">Flat</Option>
							<Option value="bungalow">Bungalow</Option>
							<Option value="terraced">Terraced</Option>
							<Option value="semi_detached">Semi_detached</Option>
							<Option value="detached">Detached</Option>
						</Select>
					</FormItem>
					<FormItem label="Floors count">
						<Select
							onChange={(e) => this.handleChangeSelect(e, 'floors')}
							value={floors}
						>
							<Option value="1">1</Option>
							<Option value="2">2</Option>
							<Option value="3">3</Option>
							<Option value="4">4</Option>
						</Select>
					</FormItem>
					<FormItem label="Reception rooms count">
						<InputNumber min={0} max={10} value={reception} style={{width: "100%"}} onChange={(e) => this.handleChangeSelect(e, 'reception')} />
					</FormItem>
					<FormItem label="Bedrooms count">
						<InputNumber min={0} max={10} value={bedrooms} style={{width: "100%"}} onChange={(e) => this.handleChangeSelect(e, 'bedrooms')} />
					</FormItem>
					<FormItem label="Number of tolets/bathroom/shower rooms">
						<InputNumber min={0} max={10} value={bathrooms} style={{width: "100%"}} onChange={(e) => this.handleChangeSelect(e, 'bathrooms')} />
					</FormItem>
					<FormItem name="checkbox" label="Type" >
						<Col span={12}>
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isBox} name="isBox">Box room</Checkbox>
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isOffice} name="isOffice">Office/Study</Checkbox>
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isConservatory} name="isConservatory">Conservatory</Checkbox>
						</Col> 
						<Col span={12}> 
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isPlay} name="isPlay">Kids play-room</Checkbox>
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isUtility} name="isUtility">Utility room</Checkbox>
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={isDining} name="isDining">Dining room</Checkbox>
						</Col>
					</FormItem>
					<ButtonGroup size="large" style={{ display: "flex", justifyContent: "center" }}>
						<Button onClick={this.handleGoBack}>
							Cancel
						</Button>
						<Button htmlType="submit" type="primary">
							Save
						</Button>
					</ButtonGroup>
				</Form>
			</Div>
		)
	}
}

export default connect(
	(state, ownProps) => {
		return {
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ editUserHousesById }, dispatch)
)(EditHouse);
