import React, { Component } from "react";
import { Modal, Button } from "antd";

class ModalMessage extends Component {
	
	componentWillReceiveProps(nextProps) {
		if (nextProps.code !== null) {		
			if (nextProps.code === 200)
				this.showModalSuccess(nextProps)
			if (nextProps.code)
				this.showModalError(nextProps);
		}
		if (nextProps.message==="User was deleted") {
			this.showModalDefault(nextProps)
		}
	}

	showModalError(props) {
		Modal.error({
			title: `Error ${props.code}`,
			content: props.message,
			onOk: () => {
				props.close();
			},
			width: 520,
			style: {
				overflowWrap: "break-word",
				wordWrap: "break-word"
			}
		});
	}
	
	showModalSuccess(props) {
		Modal.success({
			title: "Success 200",
			content: `Cleaner with the specified mail "${props.message.email}" was registered.`,
			width: 520,
			style: {
				overflowWrap: "break-word",
				wordWrap: "break-word"
			}
		});
	}
	showModalDefault(props) {
		Modal.success({
			title: "Success",
			content: props.message,
			width: 520,
			style: {
				overflowWrap: "break-word",
				wordWrap: "break-word"
			}
		});
	}

	render() {
		return <Button style={{ display: "none" }} />;
	}
}

export default ModalMessage;
