import { Modal } from "antd";

const confirm = Modal.confirm;

export const ModalDelete = (name, text, deleteItem) =>  {
	let type="delete";
	if (name==="booking") type="reject";

	return confirm({
		title: `Are you sure ${type} this ${name}?`,
		content: `${text}`,
		okText: "Yes",
		okType: "danger",
		cancelText: "No",
		onOk: () => {
			deleteItem()
		}
	});
};

export const ModalSetPaid = (setFull) => {
	return confirm({
		title: `Are you sure you want to set paid for all intervals?`,
		okText: "Yes",
		okType: "danger",
		cancelText: "No",
		onOk: () => {
			setFull()
		}
	});
}