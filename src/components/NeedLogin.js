import React, { Component } from "react";
import { Modal, Button } from "antd";

class NeedLogin extends Component {
	handleShowModal() {
		Modal.warning({
			title: "Your session has expired. Please re-login.",
			onOk: () => {
				this.props.reLogin();
			}
		});
	}
	componentDidMount() {
		this.handleShowModal();
	}

	render() {
		return <Button style={{ display: "none" }} />;
	}
}

export default NeedLogin;
