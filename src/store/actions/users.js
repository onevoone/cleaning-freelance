import {
	fetchUsers as fetchUsersRequest,
	fetchUserById as fetchUserByIdRequest,
	editUserById as editUserByIdRequest,
	createUserProfileById as createUserProfileByIdRequest,
	fetchUserHousesById as fetchUserHousesByIdRequest,
	editUserHousesById as editUserHousesByIdRequest,
	fetchApproveCleanerById as fetchApproveCleanerByIdRequest,
	putUserIntervalsById as putUserIntervalsByIdRequest,
	deleteUserIntervalsById as deleteUserIntervalsByIdRequest,
	getUserIntervalsById as getUserIntervalsByIdRequest,
	fetchEmailVerify as fetchEmailVerifyRequest,
	rebuildMonthIntervals as rebuildMonthIntervalsRerquest,
	deleteProfile as deleteProfileRequest,
	putNewPriceHour as putNewPriceHourRequest
} from "../../api";

import { setError as setMessage } from "./message";

export const SET_ARRAY_USR = "SET_ARRAY_USR";
export const SET_ARRAY_CL = "SET_ARRAY_CL";
export const SET_CONCRET_USR = "SET_CONCRET_USR";
export const SET_CONCRET_USR_HOUSES = "SET_CONCRET_USR_HOUSES";
export const SET_APPROVE_INFO = "SET_APPROVE_INFO";
export const SET_CONCRET_USR_INTERVALS = "SET_CONCRET_USR_INTERVALS";
export const SET_NEW_USR = "SET_NEW_USR";
export const SET_VIEW_TAB = "SET_VIEW_TAB";
export const SET_PRICE = "SET_PRICE";

const setUsers = (obj) => {
	return {
		type: SET_ARRAY_USR,
		payload: obj
	};
};
const setCleaners = (obj) => {
	return {
		type: SET_ARRAY_CL,
		payload: obj
	};
};
const setUserData = (obj) => {
	return {
		type: SET_CONCRET_USR,
		payload: obj
	}
}
const setItemHouse = (obj) => {
	return {
		type: SET_CONCRET_USR_HOUSES,
		payload: obj
	};
};
const setItemIntervals = (obj) => {
	return {
		type: SET_CONCRET_USR_INTERVALS,
		payload: obj
	};
};
const setNewUser = (obj) => {
	return {
		type: SET_NEW_USR,
		payload: obj
	}
}
const setViewTab = (obj) => {
	return {
		type: SET_VIEW_TAB,
		payload: obj
	}
}
const setPriceCleaner = (obj) => {
	return {
		type: SET_PRICE,
		payload: obj
	}
}
/*---------------all default users array-----------------*/
export const fetchUsers = (data) => dispatch => {
	dispatch(setUsers(null));
	return fetchUsersRequest(data)
		.then(response => dispatch(setUsers(response)))
		.catch(error => dispatch(setMessage(error)));
};
/*---------------all cleaners array-----------------*/
export const fetchCleaners = (data) => dispatch => {
	dispatch(setCleaners(null));
	return fetchUsersRequest(data)
		.then(response => dispatch(setCleaners(response)))
		.catch(error => dispatch(setMessage(error)));
};
/*--------------- concret user -----------------*/
export const fetchUserById = (id) => dispatch => {
	dispatch(setUserData(null));
	return fetchUserByIdRequest(id)
		.then(response => {
			dispatch(setUserData(response));
			return response;
		})
		.catch(error => dispatch(setMessage(error)));
}
export const editUserById = (data, id) => dispatch => {
	dispatch(setUserData(null));
	return editUserByIdRequest(data, id)
		.then(response => dispatch(setUserData(response)))
		.catch(error => dispatch(setMessage(error)));
};
export const createUserProfileById = (data) => dispatch => {
	dispatch(setNewUser(null));
	return createUserProfileByIdRequest(data)
		.then(response => {
			dispatch(setNewUser(response))
			dispatch(setMessage(response))	//log info if response 200
			//request email verify
			const verifyData = {"email": response.email}
			return fetchEmailVerifyRequest(verifyData)
				.then(response => dispatch(setNewUser(response)))
				.catch(error => dispatch(setMessage(error)));
		})
		.catch(error => dispatch(setMessage(error)));
};
/*---------------user houses-----------------*/
export const fetchUserHousesById = (id) => dispatch => {
	dispatch(setItemHouse(null));
	return fetchUserHousesByIdRequest(id)
		.then(response => {
			dispatch(setItemHouse(response))
			return response
		})
		.catch(error => dispatch(setMessage(error)));
};
export const editUserHousesById = (data, id) => dispatch => {
	dispatch(setItemHouse(null));
	return editUserHousesByIdRequest(data, id)
		.then(response => dispatch(setItemHouse(response)))
		.catch(error => dispatch(setMessage(error)));
}
/*---------------approve cleaner-----------------*/
export const fetchApproveCleanerById = (data) => dispatch => {
	return fetchApproveCleanerByIdRequest(data)
		.then(response => dispatch(setUserData(response)))
		.catch(error => dispatch(setMessage(error)));
}
/*---------------set price hour for cleaner-----------------*/
export const putNewPriceHour = (userId, data) => dispatch => {
	return putNewPriceHourRequest(userId, data)
		.then(response => dispatch(setPriceCleaner(response)))
		.catch(error => dispatch(setMessage(error)));
}
/*---------------user intervals-----------------*/
export const getUserIntervalsById = (start, end, id) => dispatch => {
	// dispatch(setItemIntervals(null));
	return getUserIntervalsByIdRequest(start, end, id)
		.then(response => dispatch(setItemIntervals(response)))
		.catch(error => dispatch(setMessage(error)));
}
export const putUserIntervalsById = (start, end, id, data) => dispatch => {
	dispatch(setItemIntervals(null));
	return putUserIntervalsByIdRequest(start, end, id, data)
		.then(response => dispatch(setItemIntervals(response)))
		.catch(error => dispatch(setMessage(error)));
}
export const deleteUserIntervalsById = (start, end, id, intervalId) => dispatch => {
	dispatch(setItemIntervals(null));
	return deleteUserIntervalsByIdRequest(start, end, id, intervalId)
		.then(response => dispatch(setItemIntervals(response)))
		.catch(error => dispatch(setMessage(error)));
}
export const rebuildMonthIntervals = (start, end, id) => dispatch => {
	dispatch(setItemIntervals(null));
	return rebuildMonthIntervalsRerquest(start, end, id)
		.then(res => dispatch(setItemIntervals(res)))
		.catch(error => dispatch(setMessage(error)));
}
export const clearIntervalsStore = () => dispatch => {
	dispatch(setItemIntervals([]));
}
/*---------------user tab view props-----------------*/
export const changeViewTab = (data) => dispatch => {
	dispatch(setViewTab(data));
}

/*---------------delete user-------------------------*/
export const deleteProfile = (id) => dispatch => {
	return deleteProfileRequest(id)
		.then(res => dispatch(setMessage(res)))
		// .catch(error => dispatch(setMessage(error)))
}