import React from 'react';
import { Form, Input, Row, Col, Spin } from "antd";
import styled from 'styled-components';

const FormItem = Form.Item;

const Inputdisabled = styled(Input)`
	cursor: default !important;
	color: rgba(0, 0, 0, 0.65) !important;
`;
const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
`;
const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
`;

export default ({data}) => {
	return <Div>
			{data 
				? <div>
					<FormItem label="Name" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Row gutter={8}>
							<Col span={12}>
								<Inputdisabled value={data.firstName} title="First name" disabled />
							</Col>
							<Col span={12}>
								<Inputdisabled value={data.lastName} title="Last name" disabled />
							</Col>
						</Row>
					</FormItem>
					<FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 16 }} label="Gender">
						<Inputdisabled value={data.sex} title="Gender" disabled />
					</FormItem>
					<FormItem label="E-mail" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Inputdisabled value={data.contactEmail} title="E-mail" disabled />
					</FormItem>
					<FormItem label="Phone" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Inputdisabled value={data.phone} title="Phone" disabled />
					</FormItem>
					<FormItem label="Address" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Inputdisabled value={data.address} title="Address" disabled />
					</FormItem>
					<FormItem label="Flat" labelCol={{ span: 5 }} wrapperCol={{ span: 16 }}>
						<Inputdisabled value={data.flat} title="Flat" disabled />
					</FormItem>
				</div> 
				: <Loading><Spin/></Loading>
			}
		</Div>;
};