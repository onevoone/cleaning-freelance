import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button, Checkbox, InputNumber, Select } from "antd";
import { addNewPrice } from "../store/actions/price";

import { emptyPrice } from "../tools/maps";

const FormItem = Form.Item;
const ButtonGroup = Button.Group;
const Option = Select.Option;

const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
	margin: 50px auto;
	border: 1px solid #d9d9d9;
	border-radius: 3px;
	padding: 25px;
`;
const H1 = styled.h1`
	width: 225px;
`;

class CreatePriceContr extends Component {
	constructor(props) {
		super(props)
		this.state = {
			...emptyPrice
		}
	}
	
	handleChangeText = (e) => {
		this.setState({ ...this.state, [e.target.name]: e.target.value });
	}
	handleCheckbox = (e) => {
		this.setState({ ...this.state, [e.target.name]: Boolean(e.target.checked) });
	}
	handleChangeAmount = (e) => {
		this.setState({ ...this.state, amount: e });
	}
	handleChangeTag = (value) => {
		this.setState({ ...this.state, tag: String(value) });
	}
	
	handleSubmit = () => {
		this.props.addNewPrice({...this.state, amount: this.state.amount*100});
		this.handleGoBack();
	}

	handleGoBack = () => this.props.router.goBack();

	render() {
		return (
			<div style={{marginTop: "20px"}}>
				<H1>New price</H1>
				<Div>
					<Form layout="vertical" style={{ width: 280}}>
						<FormItem label="Name" >
							<Input name="name" value={this.state.name} onChange={this.handleChangeText} />
						</FormItem>
						<FormItem label="Amount" >
							<InputNumber
								style={{width: "100%"}}
								min={1}
								max={9999}
								value={this.state.amount}
								// formatter={value => `£ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
								parser={value => value.replace(/\$\s?|(,*)/g, '')}
								onChange={this.handleChangeAmount}
							/>
						</FormItem>
						<FormItem label="Tag">
							<Select value={this.state.tag}  onChange={this.handleChangeTag}>
								<Option value="aditional">aditional</Option>
								<Option value="time">time</Option>
							</Select>
						</FormItem>
						<FormItem label="Description" >
							<Input name="description" value={this.state.description} onChange={this.handleChangeText} />
						</FormItem>
						<FormItem label="Multi value" >
							<Checkbox onChange={this.handleCheckbox} style={{margin: 0}} checked={this.state.isMultiValued} name="isMultiValued"></Checkbox>
						</FormItem>
						<FormItem label="Min value" >
							<Input name="minValue" value={this.state.minValue} onChange={this.handleChangeText} />
						</FormItem>
						<FormItem label="Max value" >
							<Input name="maxValue" value={this.state.maxValue} onChange={this.handleChangeText} />
						</FormItem>
						<ButtonGroup style={{ display: "flex", justifyContent: "center" }}>
							<Button onClick={this.handleGoBack}>Cancel</Button>
							<Button type="primary" onClick={this.handleSubmit}>Add</Button>
						</ButtonGroup>
					</Form>
				</Div>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ addNewPrice }, dispatch)
)(CreatePriceContr);