import styled, { css } from 'styled-components';

export const Status = styled.span`
	margin: 10px;
	padding: 4px 7px;
	border-radius: 4px;
	
	${p => p.orange && css`
		background-color: rgb(233, 173, 16);
	`}
	${p => p.red && css`
		background-color: rgb(255,41,88); 
	`}
	${p => p.green && css`
		background-color: rgb(46,187,86);
	`}
	${p => p.big && css`
		padding: 7px 14px;
		margin: 0 10px;
	`}
`;