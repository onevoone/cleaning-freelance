import {
	fetchPriceRequest,
	addNewPrice as addNewPriceRequest,
	// setHourPrice as setHourPriceRequest,
	deletePriceById as deletePriceByIdRequest,
} from "../../api";
import { setError as setMessage } from "./message";

export const SET_ARRAY_PRICE = "SET_ARRAY_PRICE";

const setPriceArr = (obj) => {
	return {
		type: SET_ARRAY_PRICE,
		payload: obj
	};
};
const setHourePrice = (obj) => {
	return {
		type: SET_ARRAY_PRICE,
		payload: obj && { ...obj, hour: obj.hour }
	};
};

export const fetchPrice = () => dispatch => {
	dispatch(setPriceArr(null));
	return fetchPriceRequest()
		.then(response => dispatch(setPriceArr(response)))
		.catch(error => dispatch(setMessage(error)));
}

export const addNewPrice = (data) => dispatch => {
	dispatch(setPriceArr(null));
	return addNewPriceRequest(data)
		.then(response => dispatch(setPriceArr(response)))
		.catch(error => dispatch(setMessage(error)))
}

// export const setHourPrice = (data) => dispatch => {
// 	return setHourPriceRequest(data)
// 		.then(response => dispatch(setHourePrice(response)))
// 		.catch(error => dispatch(setMessage(error)))
// }

export const deletePriceById = (id) => dispatch => {
	dispatch(setPriceArr(null));
	return deletePriceByIdRequest(id)
		.then(response => dispatch(setPriceArr(response)))
		.catch(error => dispatch(setMessage(error)));
}