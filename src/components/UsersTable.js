//Users.js
import React, { Fragment } from 'react';
import { Table, Icon, Button, Pagination } from "antd";
import styled from "styled-components";

import SearchForm from "./SearchForm";
import "../css/tables.css";

const DivPagination = styled.div`
	display: flex;
	justify-content: center;
	margin: 40px 0 50px 0;
`;
const H1 = styled.h1`
	width: 170px;
`;

export default ({ users, openProfile, page, search, deleteProfile }) => {

	const handleOpenProfile = (e) => openProfile(Number(e.target.id));
	const handledeleteProfile = (e) => deleteProfile(Number(e.target.id));
	const handleSearch = (data) => search(data);
	const onChangePage = (num) => page(num);
	
	const columns = [
		{ title: "ID", dataIndex: "id", className: "table-col-id" },
		{ title: "First Name", dataIndex: "firstName", className: "table-col-fname" },
		{ title: "Last Name", dataIndex: "lastName", className: "table-col-lname" },
		{ title: "Email", dataIndex: "email", className: "table-col-email" },
		{
			title: 'Action',
			className: "table-col-action",
			render: (user) => (
				<Fragment>
					<Button 
						size="small"
						type="primary" 
						id={user.id} 
						ghost 
						onClick={handleOpenProfile}
						style={{ marginRight: 10, marginBottom: 10 }}
					>
						<Icon type="profile" />View
					</Button>
					<Button
						size="small"
						id={user.id}
						onClick={handledeleteProfile}
					>
						<Icon type="user-delete" />Delete
						</Button>
				</Fragment>
			),
		},	
	];
	
	return (
		<div style={{ marginTop: "20px" }}>
			<div>
				<H1>
					Users
				</H1>
				<SearchForm find={handleSearch} />
			</div>
			<Table 
				columns={columns} 
				rowKey="id"
				pagination={false}
				loading={(users && users.items) ? false : true} 
				dataSource={users && users.items}
				scroll={{ x: 650 }}
			/>
			<DivPagination>
				<Pagination onChange={onChangePage} current={users && users.page} defaultCurrent={6} total={users && users.totalPages*10} />
			</DivPagination>
		</div>
	);
}

