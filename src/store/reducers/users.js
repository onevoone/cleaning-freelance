import {
	SET_ARRAY_USR,
	SET_ARRAY_CL,
	SET_CONCRET_USR,
	SET_CONCRET_USR_HOUSES,
	SET_CONCRET_USR_INTERVALS,
	SET_APPROVE_INFO,
	SET_NEW_USR,
	SET_VIEW_TAB,
	SET_PRICE
} from "../actions/users";


const initialState = {
	intervals: null,
	users_array: null,
	cleaners_array: null,
	user: null,
	user_houses: null,
	newCleaner: null,
	tab_view: "profile"
};

export default function users(state = initialState, action) {
	switch(action.type){
		case SET_ARRAY_USR:
			return {
				...state,
				users_array: action.payload,
			}
		case SET_ARRAY_CL:
			return {
				...state,
				cleaners_array: action.payload,
			}
		case SET_CONCRET_USR: 
			return {
				...state,
				user: action.payload,
			}
		case SET_CONCRET_USR_HOUSES:
			return {
				...state,
				user_houses: action.payload,
			}
		case SET_CONCRET_USR_INTERVALS:
			return {
				...state,
				intervals: action.payload,
			}
		case SET_APPROVE_INFO:
			return {
				...state,
				approve: action.payload,
			}
		case SET_NEW_USR:
			return {
				...state,
				newCleaner: action.payload,
			}
		case SET_VIEW_TAB:
			return {
				...state,
				tab_view: action.payload,
			}
		case SET_PRICE:
			return {
				...state,
				user: {
					...state.user, serviceHourPriceId: action.payload && action.payload.serviceHourPriceId
				}
			}
		default: 
			return state
	}
}

