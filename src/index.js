import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';

import Login from './container/Login';
import Users from './container/UsersContr';
import HocUserLoader from "./container/HocUserLoader";
// import ConcretUser from './container/ConcretUserContr';
import CreateCleaner from "./container/CreateCleanerContr";
import CreatePrice from "./container/CreatePriceContr";
import Cleaners from './container/CleanersContr';
import Price from "./container/PriceContr";
import Booking from "./container/BookingContr";
import ConcretBooking from "./container/ConcretBookingContr";
import WrapperNeedLogin from "./container/WrapperNeedLogin";
import ModalMessage from "./container/ModalMessageContr";
import NotFound from "./container/NotFound";

import './css/index.css';
import './css/login-form.css';
import 'antd/dist/antd.min.css';

import { store } from './store';

const history = syncHistoryWithStore(hashHistory, store);

const MainContainer = (
	<div className="container">
		<Provider store={store}>
			<ModalMessage>
				<Router history={history}>
					<Route path="login" component={Login}/>
					<Route path="/" component={WrapperNeedLogin}>
						<IndexRoute />
						<Route path='users' component={Users}/>
						<Route path='user/id/:userId' component={HocUserLoader}/>
						<Route path='cleaners' component={Cleaners}/>
						<Route path='cleaner/add' component={CreateCleaner}/>
						<Route path='cleaner/id/:userId' component={HocUserLoader}/>
						<Route path='booking' component={Booking}/>
						<Route path='booking/id/:bookingId' component={ConcretBooking}/>
						<Route path='settings' component={Price}/>
						<Route path='settings/add' component={CreatePrice}/>

						<Route path='*' component={NotFound}/>
					</Route>
				</Router>
			</ModalMessage>
		</Provider>
	</div>
)


ReactDOM.render(MainContainer, document.getElementById('root'));
registerServiceWorker();
