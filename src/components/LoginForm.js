// LoginForm.js
import React, { Component } from 'react';
import { Form, Icon, Input, Button, Alert } from 'antd';
const FormItem = Form.Item;

export default class LoginForm extends Component {  
	constructor(props){
		super(props);
		this.state = 
			{
				email: "",
				password: "",
				loadingBtn: false,
				...this.props,
			}
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.error) {
			this.setState({ error: nextProps.error, loadingBtn: !this.state.loadingBtn });
		}
	}
	submitForm = (event) => {
		this.setState({ error: null, loadingBtn: !this.state.loadingBtn });
		event.preventDefault();
		this.props.login(event.target[0].value, event.target[1].value);
	}
	render() {
		return (
			<div className="login-form" style={{ marginTop: "200px" }}>
				{
					this.state.error
					? <Alert message={this.state.error.message} type="error" style={{marginBottom: "20px"}} />
					: null
				}
				<Form onSubmit={this.submitForm}>
					<FormItem>
						<Input prefix={<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />} type="email" defaultValue={this.state.email} placeholder="Email" />
					</FormItem>
					<FormItem>
						<Input prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />} type="password" defaultValue={this.state.password} placeholder="Password" />
					</FormItem>
					<FormItem>
						<Button type="primary" htmlType="submit" loading={this.state.loadingBtn} className="login-form-button">
							Log in
						</Button>
					</FormItem>
				</Form>
			</div>
		);
	}
}
