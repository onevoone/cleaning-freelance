import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button } from 'antd';
import { createUserProfileById } from "../store/actions/users";

const FormItem = Form.Item;
const ButtonGroup = Button.Group;

const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
	margin: 50px auto;
	border: 1px solid #d9d9d9;
	border-radius: 3px;
	padding: 25px;
`;
const H1 = styled.h1`
	width: 225px;
`;

class CreateCleanerContr extends Component {
	constructor(props) {
		super(props)
		this.state = {
			email: null,
			password: null
		}
	}
	
	handleChangeText = (e) => {
		this.setState({ ...this.state, [e.target.name]: e.target.value });
	}
	
	handleSubmit = () => {
		this.props.createUserProfileById(this.state);
		this.handleGoBack();
	}

	handleGoBack = () => this.props.router.goBack();

	render() {
		return (
			<div style={{marginTop: "20px"}}>
				<H1>New cleaner</H1>
				<Div>
					<Form style={{width: 280}}>
						<FormItem label="E-mail" labelCol={{ span: 7 }} wrapperCol={{ span: 17 }}>
							<Input 
								name="email" 
								value={this.state.email} 
								onChange={this.handleChangeText} 
								placeholder="example@mail.com" 
							/>
						</FormItem>
						<FormItem label="Password" labelCol={{ span: 7 }} wrapperCol={{ span: 17 }}>
							<Input 
								name="password" 
								type="password" 
								value={this.state.password} 
								onChange={this.handleChangeText} 
								placeholder="qwerty1234" 
							/>
						</FormItem>
						<ButtonGroup style={{ display: "flex", justifyContent: "center" }}>
							<Button onClick={this.handleGoBack}>Cancel</Button>
							<Button type="primary" onClick={this.handleSubmit}>Add</Button>
						</ButtonGroup>
					</Form>
				</Div>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			newCl: state.newCleaner,
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ createUserProfileById }, dispatch)
)(CreateCleanerContr);