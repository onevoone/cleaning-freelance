
export const PUSH_ERROR = "PUSH_ERROR";
export const CLEAR_ERROR = "CLEAR_ERROR";

export const setError = (obj) => {
	return {
		type: PUSH_ERROR,
		payload: obj
	}
}

export const clearError = () => {
	return {
		type: CLEAR_ERROR
	}
}