// ConcretBookingContr.js
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import path from "ramda/src/path";

import ConcretBookingPage from "../components/ConcretBookingPage/";
import { ModalDelete, ModalSetPaid } from "../components/SmallModals";

import { 
	fetchBookingById, 
	rejectBookingById, 
	fetchBookingIntervalById,
	editIntervalStatusById,
	setFullPaidIntervalsById
} from "../store/actions/booking";

import { setError as setMessage } from "../store/actions/message";

class ConcretBookingContr extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fetching: false
		}
	}

	componentDidMount() {
		const bookingId = this.props.router.params.bookingId;
		if (!this.props.booking || this.props.booking.id !== Number(bookingId)) {
			this.fetchBooking(bookingId);
		}
		// if there was a transition from the concret cleaner / check if intervals has actual data
		const int = this.props.booking && this.props.booking.reservedIntervals
		if ((int && int[0] && int[0].bookingId) === undefined) {
			this.props.fetchBookingIntervalById(bookingId);
		}
	}
	componentWillReceiveProps(nextProps) {
		const nextId = Number(nextProps.params.bookingId);
		const prevId = path(["id"], this.props.booking);
		if ((prevId && nextId !== prevId) && nextProps.booking) {
			if (!this.state.fetching) {
				this.fetchBooking(nextId);
				this.setState({ fetching: true });
			}
		} else {
			this.setState({ fetching: false });
		}
	}

	fetchBooking = async (id) => {
		const reqProps = { id: Number(id) };
		await this.props.fetchBookingById(reqProps);
		this.props.fetchBookingIntervalById(id);
	}

	handleShowProfile = (type) => {
		if (type==="cleaner") {
			const userId = this.props.booking.cleaner.userId;
			this.props.router.push(`/cleaner/id/${userId}`);
		} else if (type==="user") {
			const userId = this.props.booking.user.userId;
			this.props.router.push(`/user/id/${userId}`);
		}
	}

	handleRejectBooking = () => {
		const bookingId = this.props.booking.id;
		const cleanerId = this.props.booking.cleaner.userId;
		this.rejectItem = async () => {
			await this.props.rejectBookingById(cleanerId, bookingId);
			this.props.fetchBookingIntervalById(bookingId);
		};
		ModalDelete("booking", "", this.rejectItem);
	}

	setFullPaidBooking = () => {
		const bookingId = this.props.booking.id;
		const cleanerId = this.props.booking.cleaner.userId;
		this.setItem = () => {
			this.props.setFullPaidIntervalsById(cleanerId, bookingId);
		};
		ModalSetPaid(this.setItem);
	}

	changeIntervalStatus = (intervalId, data) => {
		const bookingId = this.props.booking.id;
		this.checkAllStatuses(data, (obj) => {
			if (!obj) {
				this.props.editIntervalStatusById(bookingId, intervalId, data);
			}
		})
	}

	checkAllStatuses = (data, call) => {
		const intervals = this.props.booking.reservedIntervals;
		const bookStatus = this.props.booking.status;
		let countIntervals = null;
		for (let i = 0; i < intervals.length; i++) {
			if (intervals[i].paymentStatus === "pending" || intervals[i].paymentStatus === "failed") {
				countIntervals++
			}
		}
		
		if (bookStatus === "pending" || bookStatus === "accepted") {
			if (data.status === "paid" && countIntervals === 1) {
				this.props.setMessage({ httpCode: 400, message: "This is the final interval! Use 'Set full paid' method." })
				call(true)
			} else if (data.status === "paid" && countIntervals === 2) {
				this.props.setMessage({ httpCode: 403, message: "You need 2 or more pending(failed) intervals! To set paid for all, use 'Set full paid' method." })
				call(true)
			} else {
				call(false)
			}
		}
	}

	render() {
		return (
			<ConcretBookingPage 
				booking={this.props.booking}
				showProfile={this.handleShowProfile}
				reject={this.handleRejectBooking}
				fullPaid={this.setFullPaidBooking}
				intervalStatus={this.changeIntervalStatus}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			booking: state.booking.book,
			error: state.message.data,
			router: ownProps.router
		};
	},
	dispatch =>
		bindActionCreators(
			{ 
				fetchBookingById, 
				rejectBookingById, 
				fetchBookingIntervalById,
				editIntervalStatusById,
				setFullPaidIntervalsById,
				setMessage
			}, 
			dispatch
		)
)(ConcretBookingContr);