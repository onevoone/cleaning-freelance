// abstractRequest.js
import { 
	isSessionDataValid,
	isSessionExpire,
} from '../tools/sessionValidation';
import { store } from '../store/';
import { setData as setSession } from '../store/actions/session';
import { setError } from "../store/actions/message";
import { API_ENDPOINT } from '../constans/config';

export const simpleRequest = async (route, method, data) => {
	const fetchOptions = {
		method: 'POST',
		headers: { 'Content-Type':'application/json' },
		body: JSON.stringify(data),
	}
	const req = await fetch(API_ENDPOINT+route, fetchOptions)
	return checkApiError(req)
}

// const refreshSession = (data) => (
// 	fetch(`${API_ENDPOINT}/session/refresh`, data)
// 	.then(response => { return response.json() })
// )
export const refreshSession = async (data) => {
	const response = await fetch(`${API_ENDPOINT}/session/refresh`, data)
	return response.json()
}

async function getValidSession() {
	const currentSession = store.getState().session.data;

	if (!isSessionDataValid(currentSession)) {

		return Promise.reject(new Error('session was deleted'))
	
	} else if (isSessionExpire(currentSession)) {
		let fetchOptions = { 
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Refresh-Token": currentSession.refreshToken
			},
			body: JSON.stringify({
				"version": "string",
				"platform": "web",
				"pushToken": "",
				"role": "admin",
				"expiresIn": 0
			})
		}
		try {
			const response = await refreshSession(fetchOptions)
			store.dispatch(setSession(response))
			return response
		} catch (error) {
			store.dispatch(setError(error))
		}
	} else {
		return Promise.resolve(currentSession);
	}
}

// export function sessionRequest(route, method, data) {
// 	return getValidSession()
// 	.then( session => {
// 		let fetchOptions = {
// 			method: method,
// 			headers: { 
// 				'Content-Type':'application/json',
// 				'Auth-Token': session.authToken,
// 			}
// 		}
// 		if (method === "POST" || "PUT") {
// 			fetchOptions.body = JSON.stringify(data);
// 		}
// 		return fetch(API_ENDPOINT+route, fetchOptions)
// 	})
// 	.then( response => checkApiError(response) )
// }

export async function sessionRequest(route, method, data) {
	const valid = await getValidSession()
	let fetchOptions = {
		method: method,
		headers: { 
			'Content-Type':'application/json',
			'Auth-Token': valid.authToken,
		}
	}
	if (method === "POST" || "PUT") {
		fetchOptions.body = JSON.stringify(data);
	}
	try {
		const req = await fetch(API_ENDPOINT + route, fetchOptions);
		return checkApiError(req);
	} catch (error) {
		const data = {
			httpCode: "network",
			message: "Check your connection to internet."
		}
		store.dispatch(setError(data));
	}
}

const checkApiError = (response) => {
	if (response.ok) {
		return response.json();
	} else {
		response.json().then(error => {
			store.dispatch(setError(error));
		});
	}
}