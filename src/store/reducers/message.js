import { 
	PUSH_ERROR,
	CLEAR_ERROR
} from "../actions/message";

const initialState = {
	data: null,
}
export default function errors(state = initialState, action) {
	switch(action.type) {
		case PUSH_ERROR:
			return {
				...state,
				data: action.payload
			}
		case CLEAR_ERROR:
			return {
				data: null
			}
		default:
			return state;
	}
}