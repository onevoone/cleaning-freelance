import React, { Component } from "react";
import { Icon, Button, Input, DatePicker, Select } from "antd";
import styled from "styled-components";

const InputGroup = Input.Group;
const Option = Select.Option;
const { RangePicker } = DatePicker;

const SForm = styled.form`
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
	align-items: center;
	max-width: 690px;
	margin: 20px 0;
	@media (max-width: 767px) {
		max-width: 300px;
	}
`;

class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dateMin: null,
			dateMax: null,
			text: null,
			searchBy: "Name",
		}
	}
	
	handleChangeTypeSearch = (val) => { this.setState({ searchBy: String(val) }) };

	onChangeSearchName = (e) => { this.setState({ text: String(e.target.value) }) };

	onChangeDateRange = (date) => {
		if (date.length === 0) {
			this.setState({ dateMax: null, dateMin: null });
		} else {
			let dateMin = Number(new Date(date[0] / 1000));
			let dateMax = Number(new Date(date[1] / 1000));
			this.setState({ dateMax, dateMin });
		}
	};
	handleSearch = (e) => {
		e.preventDefault();
		this.props.find(this.state);
	};

	render() {
		return (
			<SForm onSubmit={this.handleSearch}>
				<InputGroup compact style={{width: "270px", marginBottom: "5px"}}>
					<Select value={this.state.searchBy} onChange={this.handleChangeTypeSearch} style={{minWidth: 92}}>
						<Option value="ID">ID</Option>
						<Option value="Name">Name</Option>
						{/* <Option value="Address">Address</Option> */}
					</Select>
					<Input 
						onChange={this.onChangeSearchName}
						prefix={<Icon type="search" style={{color:'rgba(0,0,0,.25)'}}/>}
						style={{width: "175px"}}
					/>
				</InputGroup>
				<RangePicker onChange={this.onChangeDateRange} style={{width: "267px", marginBottom: "5px"}} format="DD-MM-YYYY" />
				<Button htmlType="submit" style={{marginBottom: "5px"}} icon="search">Search</Button>
			</SForm>
		)
	}
}

export default SearchForm;