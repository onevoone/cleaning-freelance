// ConcretUserContr.js
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import {
	fetchApproveCleanerById,
	changeViewTab,
} from "../store/actions/users";
import { fetchBookingById } from "../store/actions/booking";
import { putNewPriceHour } from "../store/actions/users";
import { setError as setMessage } from "../store/actions/message";

import ConcretUserPage from "../components/ConcretUserPage/";

class ConcretUserContr extends Component {
	
	// shouldComponentUpdate(nextProps, nextState) {
	// 	if (this.props.currentUser.data !== nextProps.currentUser.data ||
	// 		this.props.currentUser.user_houses !== nextProps.currentUser.user_houses ||
	// 		this.props.currentUser.approve !== nextProps.currentUser.approve ||
	// 		this.props.currentUser.intervals !== nextProps.currentUser.intervals ||
	// 		this.props.currentUser.tab_view !== nextProps.currentUser.tab_view ) {
	// 		return true;
	// 	}
	// 	return false
	// }
	handleSetNewHourPrice = (id) => {
		const data = { "serviceHourPriceId": id };
		this.props.putNewPriceHour(this.props.currentUser.data.id, data);
	}
	handleApproveCleaner = (data) => {
		this.props.fetchApproveCleanerById(data);
	}
	handleChangeView = (newView) => {
		this.props.changeViewTab(newView);
	}
	handleGoBack = () => {
		this.props.router.goBack();
	}
	handleRedirectToBooking = (id) => {
		this.props.fetchBookingById({ equals: id })
			.then(res => {
				const bookingId = res.items[0].id;
				this.props.router.push(`/booking/id/${bookingId}`);
			})
			.catch(error => this.props.setMessage(error))
	}

	render() {
		const { currentUser, price } = this.props;
		return (
			<ConcretUserPage
				user={currentUser.data}
				changeApprove={this.handleApproveCleaner}
				intervals={currentUser.intervals}
				houses={currentUser.user_houses || null}
				view={currentUser.tab_view}
				changeView={this.handleChangeView}
				goBack={this.handleGoBack}
				showBooking={this.handleRedirectToBooking}
				price={price}
				setNewPrice={this.handleSetNewHourPrice}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			currentUser: {
				data: state.users.user,
				intervals: state.users.intervals,
				user_houses: state.users.user_houses,
				tab_view: state.users.tab_view
			},
			price: state.price.data,
			session: state.session,
			error: state.message.data,
			router: ownProps.router
		};
	},
	dispatch =>
		bindActionCreators(
			{
				fetchApproveCleanerById,
				changeViewTab,
				fetchBookingById,
				setMessage,
				putNewPriceHour
			},
			dispatch
		)
)(ConcretUserContr);