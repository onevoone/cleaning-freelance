import React from 'react';
import { Form, Input, Col, Spin, Checkbox } from "antd";
import styled from 'styled-components';

const FormItem = Form.Item;

const Inputdisabled = styled(Input)`
	cursor: default !important;
	color: rgba(0, 0, 0, 0.65) !important;
`;
const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	align-items: center;
`;
const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
`;

export default ({data}) => {
	return <Div>
			{data && data.id 
				? <Form layout="vertical" style={{maxWidth: "350px"}}>
					<FormItem label="Name">
						<Inputdisabled value={data.name} title="name" disabled />
					</FormItem>
					<FormItem label="Address">
						<Inputdisabled value={data.address} title="Address" disabled />
					</FormItem>
					<FormItem label="Flat">
						<Inputdisabled value={data.flat} title="Flat" disabled />
					</FormItem>
					<FormItem label="Type">
						<Inputdisabled value={data.type} title="Type" disabled />
					</FormItem>
					<FormItem label="Floors count">
						<Inputdisabled value={data.floors} title="Floors count" disabled />
					</FormItem>
					<FormItem label="Reception rooms count">
						<Inputdisabled value={data.reception} title="Reception rooms count" disabled />
					</FormItem>
					<FormItem label="Bedrooms count">
						<Inputdisabled value={data.bedrooms} title="Bedrooms count" disabled />
					</FormItem>
					<FormItem label="Number of tolets/bathroom/shower rooms" style={{textOverflow:"ellipsis",whiteSpace:"nowrap",overflow:"hidden"}}>
						<Inputdisabled value={data.bathrooms} title="Number of tolets/bathroom/shower rooms" disabled />
					</FormItem>
					<FormItem label="Type">
						<Col span={12}>
							<Checkbox checked={data.isBox} style={{margin: 0}}>Box room</Checkbox>
							<Checkbox checked={data.isOffice} style={{margin: 0}}>Office/Study</Checkbox>
							<Checkbox checked={data.isConservatory} style={{margin: 0}}>Conservatory</Checkbox>
						</Col>
						<Col span={12}>
							<Checkbox checked={data.isPlay} style={{margin: 0}}>Kids play-room</Checkbox>
							<Checkbox checked={data.isUtility} style={{margin: 0}}>Utility room</Checkbox>
							<Checkbox checked={data.isDining} style={{margin: 0}}>Dining room</Checkbox>
						</Col>
					</FormItem>
				</Form> 
				: <Loading><Spin/></Loading>
			}
		</Div>;
};