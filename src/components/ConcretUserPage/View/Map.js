import React, { Component } from "react";
import {
	withScriptjs,
	withGoogleMap,
	GoogleMap,
	Marker,
} from "react-google-maps";
import { compose, withProps } from "recompose";

const MyMapComponent = compose(
	withProps({
		googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCPBrCPvOOwClUuu2AXs6AGlsrxsr4pn_g&v=3.exp&libraries=geometry,drawing,places",
		loadingElement: <div style={{ height: `100%` }} />,
		containerElement: <div style={{ height: `378px` }} />,
		mapElement: <div style={{ height: `100%` }} />
	}),
	withScriptjs,
	withGoogleMap
)(props => {
	return (
		<GoogleMap defaultZoom={17} defaultCenter={props.point}>
			<Marker position={props.point} />
		</GoogleMap>
	);
});

export default class Map extends Component {

	render() {
		const { user } = this.props;
		
		return user.location && user.location.lat && (
			<div>
				{ <MyMapComponent point={{ lat: user.location.lat, lng: user.location.lng }} /> }
			</div>
		);
	}
}