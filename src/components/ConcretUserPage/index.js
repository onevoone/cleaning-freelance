// ConcretUserPage.js
import React, { Component } from 'react';
import { Icon, Button, Tabs, Modal, Select } from "antd";
import styled from "styled-components";
import path from "ramda/src/path";

import { 
	togglerCleanerApprove, tabProfileRender, 
	tabHouseRender, tabIntervalsRender, tabMapRender,
	serviceHourPrice
} from "./RenderItems";

import { centToPrice } from "../../tools/maps";

const Option = Select.Option;
const Div = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-content: center;
	margin: 50px auto;
	border: 1px solid #d9d9d9;
	border-radius: 3px;
	padding: 25px;
`;

class ConcretUserPage extends Component {
	constructor(props) {
		super(props)
		this.state = {
			editProfile: false,
			editHouse: false,
			editIntervals: false,
			modal: false,
			newPrice: null
		}
	}
	
	handleChangeDetail = (details) => {
		// switch (details) {
		// 	case "profile":
		// 		this.props.changeView(details); break;
		// 	case "intervals":
		// 		this.props.changeView(details); break;
		// 	case "map":
		// 		this.props.changeView(details); break;
		// 	default: // if house
		// 		this.props.changeView(Number(details)); break;
		// }
		if (Number(details)) //if house
			this.props.changeView("house");
		else
			this.props.changeView(details);
		
	}
	handleApproveCleaner = (val) => {
		let data = { "userId": this.props.user.id, "isCleanerApproved": val }
		this.props.changeApprove(data)
	}
	handleEditUser = () => {
		switch (this.props.view) {
			case "profile": 
				this.setState({ editProfile: !this.state.editProfile }); break;
			case "intervals":
				this.setState({ editIntervals: !this.state.editIntervals }); break;
			default:
				this.setState({ editHouse: !this.state.editHouse }); break;
		}	
	}
	handleCancelEdit = () => {
		this.setState({ editProfile: false, editHouse: false })
	}
	handleRedirectToBooking = (data) => this.props.showBooking(data);

	/**
	 * modal - set new hour price for cleaner
	 */
	handleSetPriceModal = () => {
		this.setState({ modal: !this.state.modal });
	}
	handleSelectNewPrice = (id) => {
		this.setState({ newPrice: id });
	}
	handleOk = async () => {
		await this.props.setNewPrice(this.state.newPrice);
		this.handleSetPriceModal();
	}
	hand

	render() {
		const { editProfile, editHouse, editIntervals, modal, newPrice } = this.state;
		const { user, view, price } = this.props;
		
		const handleEdit = (
			<Button onClick={this.handleEditUser} title="Edit profile or house">
				<Icon type="edit"/>
				{editProfile || editHouse || editIntervals ? "Cancel" : "Edit"}
			</Button>
		);
		
		return (
			<Div>
				<h2>
					{path(["firstName"], user)} {path(["lastName"], user)}
				</h2>
				<div style={{display: "flex", alignItems: "center", margin: "10px 0"}}>
					{ togglerCleanerApprove(this.props, this.handleApproveCleaner) }
					{ serviceHourPrice(this.props, this.handleSetPriceModal) }
				</div>
				<Tabs 
					tabBarExtraContent={ 
						(view === "profile" || view === "intervals" || view === "house")
						? handleEdit 
						: null 
					} 
					defaultActiveKey={this.props.view} 
					onChange={this.handleChangeDetail}
					forceRender={true}
				>
					{ tabProfileRender(this.state, this.props, this.handleCancelEdit) }
					{ tabHouseRender(this.state, this.props, this.handleCancelEdit) }
					{ tabIntervalsRender(this.state, this.props, this.handleRedirectToBooking) }
					{ tabMapRender(this.props) }
				</Tabs>

				<Modal 
					title="Select new hour price for cleaner"
					visible={modal}
					onOk={this.handleOk}
					onCancel={this.handleSetPriceModal}
				>
					<Select value={newPrice} style={{width: "100%"}} onChange={this.handleSelectNewPrice}>
						{
							price && price.time.map((obj, k) => (
								<Option key={k} value={obj.id}>{centToPrice(obj.amount)}, {obj.name}</Option>
							))
						}
					</Select>
				</Modal>
			</Div>
		)
	}
}

export default ConcretUserPage;