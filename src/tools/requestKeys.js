
/******************************/
//
// =>	admin/bookingQuery
//
/******************************/
[
	"auth_us.is_email_verified",
	"auth_us.email",
	"auth_us.fbId",

	"auth_cl.is_email_verified",
	"auth_cl.email",
	"auth_cl.fbId",

	"profile_us.id",
	"profile_us.first_name",
	"profile_us.last_name",
	"profile_us.address",
	"profile_us.flat",
	"profile_us.phone",
	"profile_us.contact_email",
	"profile_us.updated_at",
	"profile_us.created_at",

	"profile_cl.id",
	"profile_cl.first_name",
	"profile_cl.last_name",
	"profile_cl.address",
	"profile_cl.flat",
	"profile_cl.phone",
	"profile_cl.contact_email",
	"profile_cl.updated_at",
	"profile_cl.created_at",

	"cleaner.id",
	"cleaner.lat",
	"cleaner.lng",
	"cleaner.is_cleaner_approved",

	"booking.id",
	"booking.status",
	"booking.amount",
	"booking.comment",
	"booking.created_at",

	"interval.time",
	"interval.duration",
];


/******************************/
//
// =>	admin/usersQuery
//
/******************************/
[
	"user.full_name",
	"user.full_address",

	"user.id",
	"user.first_name",
	"user.last_name",
	"user.address",
	"user.flat",
	"user.phone",
	"user.contact_email",
	"user.updated_at",
	"user.created_at",

	"cleaner.id",
	"cleaner.lat",
	"cleaner.lng",
	"cleaner.is_Cleaner_approved",

	"admin.id",

	"authentication.is_email_verified",
	"authentication.email",
	"authentication.fbId"
];