// ConcretBookingPage.js
import React from "react";
import { Icon, Spin, Collapse, Button } from "antd";
import styled from "styled-components";

import ListIntervals from "./ListIntervals";
import { Status } from "./StatusSpan";

import { centToPrice } from "../../tools/maps";
import "../../css/ConcretBookingPage.css";

const Panel = Collapse.Panel;
const Loading = styled.div`
	margin: 50px auto;
	width: 30px;
`;

export default ({ booking, showProfile, reject, fullPaid, intervalStatus }) => {

	const goToProfile = (event) => {
		const type = String(event.target.name);
		showProfile(type);
	}
	const rejectBooking = () => reject();

	const setFullPaid = () => fullPaid();

	const changeIntervalStatus = (id, data) => intervalStatus(id, data);

	return <div className="block_div">
		{booking
			? <div>
				<div className="head">
					<p>
						<span style={{ fontWeight: 500 }}>Booking Id:</span>
						<span style={{ margin: 10 }}>
							{booking.id}
						</span>
					</p>
					<p>
						<span style={{ fontWeight: 500 }}>Amount:</span>
						<span style={{ margin: 10 }}>
							{booking.payment.amount / 100} &pound;
						</span>
					</p>
					<div>
						<span style={{ fontWeight: 500 }}>Status:</span>
						{
							booking.status === "pending"
								? <Status big orange>{booking.status}</Status>
								: booking.status === "accepted" || booking.status === "completed"
									? <Status big green>{booking.status}</Status>
									: <Status big red>{booking.status}</Status>
						}
						<div>
							{
								booking.status !== "rejected"
									? <Button style={{marginRight: 10}} type="dashed" onClick={rejectBooking}>Reject</Button>
									: null
							}
							{
								booking.status === "pending" || booking.status === "accepted"
									? <Button type="dashed" onClick={setFullPaid}>Set full paid</Button>
									: null
							}
						</div>
					</div>
				</div>

				<div className="divider">
					<span>Comment</span>
				</div>
				<div className="list">{booking.comment}</div>

				<div className="divider">
					<span>Cleaner</span>
				</div>
				<div className="list">
					<p className="text">
						<span>First name:</span> {booking.cleaner.firstName}
					</p>
					<p className="text">
						<span>Last name:</span> {booking.cleaner.lastName}
					</p>
					<p className="text">
						<span>Email:</span> {booking.cleaner.email}
					</p>
					<a className="link_profile" name="cleaner" id={booking.cleaner.userId} onClick={goToProfile}>
						Show profile
					</a>
				</div>

				<div className="divider">
					<span>User</span>
				</div>
				<div className="list">
					<p className="text">
						<span>First name:</span> {booking.user.firstName}
					</p>
					<p className="text">
						<span>Last name:</span> {booking.user.lastName}
					</p>
					<p className="text">
						<span>Email:</span> {booking.user.email}
					</p>
					<a className="link_profile" name="user" id={booking.userId} onClick={goToProfile}>
						Show profile
					</a>
				</div>

				<div className="divider">
					<span>House</span>
				</div>
				<Collapse bordered={false}>
					<Panel
						header={booking.house.name || "House 1"}
						key="1">
						<div className="list">
							<p className="text">
								<span>Name:</span> {booking.house.name}
							</p>
							<p className="text">
								<span>Address:</span> {booking.house.address}
							</p>
							<p className="text">
								<span>Flat:</span> {booking.house.flat}
							</p>
							<p className="text">
								<span>Type:</span> {booking.house.type}
							</p>
							<p className="text">
								<span>Floors count:</span> {booking.house.floors}
							</p>
							<p className="text">
								<span>Reception rooms count:</span> {booking.house.reception}
							</p>
							<p className="text">
								<span>Bedrooms count:</span> {booking.house.bedrooms}
							</p>
							<p className="text">
								<span>Number of tolets/bathroom/shower rooms:</span>
								{booking.house.bathrooms}
							</p>
							<p className="text">
								<span>Box room:</span>
								{
									booking.house.isBox
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
							<p className="text">
								<span>Office/Study:</span>
								{
									booking.house.isOffice
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
							<p className="text">
								<span>Conservatory</span>
								{
									booking.house.isConservatory
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
							<p className="text">
								<span>Kids play-room:</span>
								{
									booking.house.isPlay
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
							<p className="text">
								<span>Utility room:</span>
								{
									booking.house.isUtility
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
							<p className="text">
								<span>Dining room:</span>
								{
									booking.house.isDining
										? <Icon type="plus-circle-o" />
										: <Icon type="minus-circle-o" />
								}
							</p>
						</div>
					</Panel>
				</Collapse>

				<div className="divider">
					<span>Services</span>
				</div>
				<Collapse bordered={false}>
					{booking.services.map((obj, k) => (
						<Panel header={obj.name} key={k}>
							<div className="list">
								<p className="text">
									<span>Name:</span> {obj.name}
								</p>
								<p className="text">
									<span>Amount:</span> <span>&pound; {centToPrice(obj.amount)}</span>
								</p>
								<p className="text">
									<span>Tag:</span> {obj.tag}
								</p>
								<p className="text">
									<span>Description:</span> {obj.description}
								</p>
								<p className="text">
									<span>Multi valued:</span>
									{
										obj.isMultiValued
											? <Icon type="plus-circle-o" />
											: <Icon type="minus-circle-o" />
									}
								</p>
								<p className="text">
									<span>Min value:</span> {obj.minValue}
								</p>
								<p className="text">
									<span>Max value:</span> {obj.maxValue}
								</p>
							</div>
						</Panel>
					))}
				</Collapse>

				<div className="divider">
					<span>Intervals</span>
				</div>
				<div className="list">
					<ListIntervals 
						intervals={booking.reservedIntervals} 
						intervalStatus={changeIntervalStatus} 
						bookingStatus={booking.status} 
					/>
				</div>
			</div>
			: <Loading><Spin /></Loading>}
	</div>;
}
