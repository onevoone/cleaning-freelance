import React from "react";
import { connect } from "react-redux";

import { isSessionDataValid } from "../tools/sessionValidation";
import Navigationbar from "./Navigationbar";
import NeedLogin from "../components/NeedLogin";

const WrapperNeedLogin = ({ children, session, router }) => {
	if (isSessionDataValid(session)) {
		return <Navigationbar router={router}>{children}</Navigationbar>;
	} else {
		return <NeedLogin reLogin={() => router.replace("/login")} />;
	}
};

export default connect((state, ownProps) => {
	return {
		router: ownProps.router,
		session: state.session.data
	};
})(WrapperNeedLogin);
