import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
	fetchPrice,
	deletePriceById
} from "../store/actions/price";

import PriceTable from "../components/PriceTable";

class SettingsContr extends Component {
	componentDidMount() {
		if (this.props.price === null) {
			this.props.fetchPrice();
		}
	}

	handleCreatePrice = () => this.props.router.push(`/settings/add`);
	handleDeletePrice = (id) => this.props.deletePriceById(id);

	render() {
		return (
			<PriceTable
				price={this.props.price}
				openService={this.handleOpenService}
				addPrice={this.handleCreatePrice}
				deletePrice={this.handleDeletePrice}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			price: state.price.data,
			router: ownProps.router
		};
	},
	dispatch =>
		bindActionCreators(
			{
				fetchPrice,
				deletePriceById
			},
			dispatch
		)
)(SettingsContr);