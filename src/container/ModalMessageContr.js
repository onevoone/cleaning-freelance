import React from 'react';
import { connect } from "react-redux"; 
import { bindActionCreators } from 'redux' 

import ModalMessage from "../components/ModalMessage";
import { clearError } from "../store/actions/message";

const ModalMessageContr = ({ children, error, clearError }) => {

	if (error && error.authToken) {
		return (
			<div>
				{<ModalMessage code={200} message={error} close={() => clearError()} /> }
				{children}
			</div>
		);
	} else {
		const hCode = error && error.httpCode;
		const message = error && error.message;
		return (
			<div>
				{<ModalMessage code={hCode} message={message} close={() => clearError()} /> }
				{children}
			</div>
		);
	}
};

export default connect(
	(state, ownProps) => {
		return {
			error: state.message.data
		};
	},
	dispatch => bindActionCreators({ clearError }, dispatch)
)(ModalMessageContr);
