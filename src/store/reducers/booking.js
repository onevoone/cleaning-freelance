import { 
	SET_ARRAY_BOOKING, 
	SET_CONCRET_BOOKING,
	SET_BOOKING_INTERVALS
} from "../actions/booking";

const initialState = {
	array: null,
	book: null
};

export default function(state = initialState, action) {
	switch (action.type) {
		case SET_ARRAY_BOOKING:
			return {
				...state,
				array: action.payload
			}
		case SET_CONCRET_BOOKING:
			return {
				...state,
				book: action.payload && action.payload.items && action.payload.items[0]
			}
		case SET_BOOKING_INTERVALS:
			return {
				...state,
				book: {
					...state.book, reservedIntervals: action.payload
				}
			}
		default:
			return state;
	}
}
