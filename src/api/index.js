import { simpleRequest, sessionRequest } from './abstractRequest';
// import { requestKeys } from '../tools/maps';

import { checkRequestUserData, checkRequestBookingData } from "../tools/maps";

export const login = (email, password) => simpleRequest(
	'/session/login',
	'POST',
	{
		"authentication": {
			"type": "email",
			"email": email,
			"password": password,
			"fbToken": ""
		},
		"sessionConfig": {
			"version": "string",
			"platform": "web",
			"pushToken": "string",
			"role": "admin",
			"expiresIn": 0
		}
	}
)
/* ------------------------ Users --------------------------- */
export const fetchUsers = (data) => sessionRequest(
	`/admin/usersQuery`,
	`POST`,
	checkRequestUserData(data)
)
export const fetchUserById = (id) => sessionRequest(
	`/admin/user/id/${id}`,
	`GET`
)
export const editUserById = (data, id) => sessionRequest(
	`/admin/user/id/${id}`,
	`PUT`,
	data
)
export const createUserProfileById = (data) => simpleRequest(
	'/session/register',
	'POST',
	{
		"authentication": {
			"type": "email",
			"email": data.email,
			"password": data.password,
			"fbToken": ""
		},
		"sessionConfig": {
			"version": "string",
			"platform": "web",
			"pushToken": "string",
			"role": "cleaner",
			"expiresIn": 0
		}
	}
)
export const fetchEmailVerify = (data) => sessionRequest(
	`/email/send/verify.email`,
	`POST`,
	data
)
export const fetchUserHousesById = (id) => sessionRequest(
	`/user/userId/${id}/house`,
	`GET`
)
export const editUserHousesById = (data, id) => sessionRequest(
	`/user/userId/${id}/house`,
	`PUT`,
	data
)
export const fetchApproveCleanerById = (data) => sessionRequest(
	`/admin/cleanerApproved`,
	`POST`,
	data
)
export const getUserIntervalsById = (start, end, id) => sessionRequest(
	`/cleaner/userId/${id}/intervals?start=${start}&end=${end}`,
	`GET`
)
export const putUserIntervalsById = (start, end, id, data) => sessionRequest(
	`/cleaner/userId/${id}/intervals?start=${start}&end=${end}`,
	`PUT`,
	data
)
export const deleteUserIntervalsById = (start, end, id, intervalId) => sessionRequest(
	`/cleaner/userId/${id}/intervals/id/${intervalId}?start=${start}&end=${end}`,
	`DELETE`
)
export const rebuildMonthIntervals = (start, end, id) => sessionRequest(
	`/cleaner/userId/${id}/intervals/rebuild?start=${start}&end=${end}`,
	`GET`
)
export const deleteProfile = (userId) => sessionRequest(
	`/admin/profile/id/${userId}`,
	`DELETE`
)
export const putNewPriceHour = (userId, data) => sessionRequest(
	`/cleaner/userId/${userId}/serviceHourPriceId`,
	`PUT`,
	data
)
/* ------------------------ settings --------------------------- */
export const fetchPriceRequest = () => sessionRequest(
	`/settings`,
	`GET`,
)
export const addNewPrice = (data) => sessionRequest(
	`/admin/service`,
	`POST`,
	data
)
// export const setHourPrice = (data) => sessionRequest(
// 	`/admin/service/setHourPrice/${data}`,
// 	`GET`
// )
export const deletePriceById = (id) => sessionRequest(
	`/admin/service/id/${id}`,
	`DELETE`
)
/* ------------------------ Booking --------------------------- */
export const fetchBookingArr = (data) => sessionRequest(
	`/admin/bookingQuery`,
	`POST`,
	checkRequestBookingData(data)
)
export const fetchBookingById = (data) => sessionRequest(
	`/admin/bookingQuery`,
	`POST`,
	checkRequestBookingData(data)
)
export const fetchBookingIntervalById = (bookingId) => sessionRequest(
	`/admin/booking/${bookingId}/interval`,
	`GET`
)
export const editIntervalStatusById = (bookingId, intervalId, data) => sessionRequest(
	`/admin/booking/${bookingId}/interval/id/${intervalId}`,
	`POST`,
	data
)
export const setFullPaidIntervalsById = (cleanerId, bookingId) => sessionRequest(
	`/admin/cleaner/userId/${cleanerId}/booking/id/${bookingId}/setFullPaid`,
	`GET`
)
export const rejectBookingById = (cleanerId, bookingId) => sessionRequest(
	`/admin/cleaner/userId/${cleanerId}/booking/id/${bookingId}/reject`,
	`GET`
)
