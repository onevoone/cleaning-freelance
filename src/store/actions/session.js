import Cookies from "js-cookie";

// import { cookieKeys } from '../../constans/cookieKeys';
import { login as loginRequest } from "../../api/";

import { setError } from "./message";

export const CHECK_LOGIN = "CHECK_LOGIN";
export const CLEAR = "CLEAR";
// export const PUSH_ERROR = "PUSH_ERROR";

const cookieKeys = {
	authToken: "AUTH_TOKEN",
	createdAt: "SESSION_CREATED_TIME",
	expiresIn: "SESSION_EXPIRES_TIME",
	refreshToken: "REFRESH_TOKEN",
	scToken: "SELF_CONTAINED_TOKEN",
	userId: "USER_ID"
};

export const setData = obj => {
	writeCookies(obj);
	return {
		type: CHECK_LOGIN,
		payload: obj
	};
};
export const clearData = () => {
	removeCookies();
	return {
		type: CLEAR
	};
};
export const login = (mail, pass) => async dispatch => {
	try {
		const response = await loginRequest(mail, pass);
		if (response && response.httpCode){
			dispatch(setError(response))
		} else if (response && response.role === "user") {
			dispatch(setError({httpCode: "Wrong account", message: "You are using a user account. use your admin account"}));
		} else if (response===undefined) {
			return
		} else {
			dispatch(setData(response));
		}
	} catch (error) {
		dispatch(setError(error));
	}
};

/* Cookies */

export function reedCookies() {
	let cookieKeys = {
		authToken: "AUTH_TOKEN",
		createdAt: "SESSION_CREATED_TIME",
		expiresIn: "SESSION_EXPIRES_TIME",
		refreshToken: "REFRESH_TOKEN",
		scToken: "SELF_CONTAINED_TOKEN",
		userId: "USER_ID"
	};
	let obj = {};
	Object.keys(cookieKeys).forEach((key, index) => {
		obj[key] = Cookies.get(String(cookieKeys[key]));
	});
	obj.userId = Number(obj.userId);
	obj.expiresIn = Number(obj.expiresIn);
	return obj;
}

const removeCookies = () => {
	Object.keys(cookieKeys).forEach((key, index) => {
		Cookies.remove(cookieKeys[key]);
	});
};

const writeCookies = session => {
	const cfg = { expires: 3 };
	Object.keys(cookieKeys).forEach((key, index) => {
		Cookies.set(cookieKeys[key], session[key], cfg);
	});
};
