import { combineReducers } from 'redux';
import session from './session';
import users from './users';
import price from "./price";
import message from "./message";
import booking from "./booking";
import { routerReducer } from 'react-router-redux';
export default combineReducers({
	routing: routerReducer,
	session,
	users,
	price,
	message,
	booking,
});