// CleanersContr.js
import React, { Component } from 'react';
import { bindActionCreators } from 'redux' 
import { connect } from 'react-redux';

import CleanersTable from '../components/CleanersTable';
import { fetchCleaners, changeViewTab, deleteProfile } from "../store/actions/users";

class CleanersContr extends Component {
	
	componentDidMount() {
		if (this.props.cleaners === null) {
			let reqProps = { type: "cleaners" };
			this.props.fetchCleaners(reqProps);
		}
	}

	handleAddCleaner = () => this.props.router.push(`/cleaner/add`);

	handleOpenProfile = (userId) => {
		this.props.changeViewTab("profile");
		this.props.router.push(`/cleaner/id/${userId}`);
	};
	handleDeleteProfile = (userId) => {
		this.props.deleteProfile(userId);
	};
	handleChangePage = (page) => {
		let reqProps = { type: "cleaners", page };
		this.props.fetchCleaners(reqProps);
	};
	handleSearch = (search) => {
		search.type = "cleaners";
		this.props.fetchCleaners(search);
	};

	render() {
		return (
			<CleanersTable
				addCleaner={this.handleAddCleaner}
				openProfile={this.handleOpenProfile}
				page={this.handleChangePage}
				cleaners={this.props.cleaners}
				search={this.handleSearch}
				deleteProfile={this.handleDeleteProfile}
			/>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			cleaners: state.users.cleaners_array,
			session: state.session,
			router: ownProps.router
		};
	},
	dispatch => bindActionCreators({ fetchCleaners, changeViewTab, deleteProfile }, dispatch)
)(CleanersContr);