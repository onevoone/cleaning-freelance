// Navbar.js
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Menu, Icon, Modal } from "antd";
import { clearData } from "../store/actions/session";
import { clearError } from "../store/actions/message";
import styled, { css } from "styled-components";

const confirm = Modal.confirm;

const MenuItemCustom = styled(Menu.Item)`
	@media (max-width: 767px) {
		padding: 0px 7px !important;
	}
`;
const MyIcon = styled(Icon)`
	${props => props.show==="mobile" && css`
		display: none !important;
	`}
`;

class Navigationbar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: null,
			windowSize: "desctop",
		}
	}

	componentDidMount() {
		this.updateWindow();
		this.updateCurrentRoute()
	};

	componentWillReceiveProps(nextProps) {
		this.updateWindow();
		this.updateCurrentRoute()
	}

	updateCurrentRoute = () => {
		const route = this.props.childRouter && this.props.childRouter.props.route.path;
		if (route === "cleaner/id/:userId" || route === "cleaners")
			this.setState({ current: "cleaners" });
		else if (route === "user/id/:userId" || route === "users")
			this.setState({ current: "users" });
		else if (route === "booking/id/:bookingId" || route === "booking")
			this.setState({ current: "booking" });
		else if (route === "settings")
			this.setState({ current: "settings" });
		else return
	}

	updateWindow = () => {
		if (window.innerWidth < 480)
			this.setState({ windowSize: "mobile" });
		else 
			this.setState({ windowSize: "desctop" });
	};

	handleRedirect = (e) => {
		let router = this.props.router;
		if (e.key === "logout") {
			confirm({
				title: "Are you sure you want to log out?",
				onOk: () => {
					this.props.clearData();
					this.props.clearError();
					router.replace("/login");
				}
			});
		} else {
			router.push(`/${e.key}`);
			this.setState({current: e.key});
		}
	};

	render() {
		const { current, windowSize } = this.state;
		return (
			<div>
				<Menu onClick={this.handleRedirect} mode="horizontal" selectedKeys={[current]}>
					<MenuItemCustom key="users">
						<MyIcon show={windowSize} type="user"/>
						Users
					</MenuItemCustom>
					<MenuItemCustom key="cleaners">
						<MyIcon show={windowSize} type="team"/>
						Cleaners
					</MenuItemCustom>
					<MenuItemCustom key="booking">
						<MyIcon show={windowSize} type="calculator"/>
						Booking
					</MenuItemCustom>
					<MenuItemCustom key="settings">
						<MyIcon show={windowSize} type="book"/>
						Settings
					</MenuItemCustom>
					<MenuItemCustom key="logout" style={{ float: "right" }}>
						<MyIcon show={windowSize} type="logout"/>
						Log out
					</MenuItemCustom>
				</Menu>
				{this.props.children}
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => {
		return {
			router: ownProps.router,
			childRouter: ownProps.children
		};
	},
	dispatch => bindActionCreators({ clearData, clearError }, dispatch)
)(Navigationbar);
