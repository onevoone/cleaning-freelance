import { SET_ARRAY_PRICE } from "../actions/price";

const initialState = {
	data: null
}

export default function price(state = initialState, action) {
	switch (action.type) {
		case SET_ARRAY_PRICE:
			return {
				...state,
				data: action.payload
			}
		default:
			return state;
	}
}